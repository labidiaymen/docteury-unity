const CONFIDENTIALITE = function () {
  return (
    <div className="container">
      <h1 className="c13">
        <a name="h.21ovinuz8ozk"></a>
        <span className="c9">Politique de confidentialité</span>
      </h1>

      <p className="c1">
        <span className="c0">
          Cette Politique de Confidentialit&eacute; d&eacute;finit la
          mani&egrave;re dont Docteury (ci- apr&egrave;s la &laquo;
          Soci&eacute;t&eacute; &raquo;) utilise et prot&egrave;ge les
          informations que nous recueillons lorsque vous utilisez le site
        </span>{" "}
        <span className="c9">
          <a
            className="c10"
            href="https://www.google.com/url?q=http://www.docteury.tn"
          >
            www.docteury.tn
          </a>
        </span>
        <span className="c0">
          &nbsp;(ci-apr&egrave;s le &laquo; Site &raquo;) et
          b&eacute;n&eacute;ficiez du service de prise de rendez-vous
          m&eacute;dicaux en ligne (ci-apr&egrave;s le &laquo; Service &raquo;).
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          La protection de vos donn&eacute;es personnelles est une
          pr&eacute;occupation constante pour nous, et la Soci&eacute;t&eacute;
          s&rsquo;engage donc par la pr&eacute;sente &agrave; faire en sorte que
          :
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          (i) Seules les informations personnelles n&eacute;cessaires au bon
          fonctionnement du Service soient collect&eacute;es
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          (ii) Vos informations priv&eacute;es soient s&eacute;curis&eacute;es
          et prot&eacute;g&eacute;es
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          La Soci&eacute;t&eacute; se r&eacute;serve la facult&eacute; de
          modifier la pr&eacute;sente Politique de Confidentialit&eacute;
          &agrave; tout moment sans pr&eacute;avis. Vous devez v&eacute;rifier
          cette page de temps en temps pour s&rsquo;assurer que vous avez
          pleinement connaissance des dispositions de cette Politique de
          Confidentialit&eacute; et que vous &ecirc;tes en accord avec
          celles-ci. Si vous n&rsquo;acceptez pas ces dispositions, vous devez
          cesser d&rsquo;utiliser le Site et renoncer &agrave;
          b&eacute;n&eacute;ficier du Service propos&eacute;.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Cette Politique de Confidentialit&eacute; est effective &agrave;
          partir du 25 avril 2013.
        </span>
      </p>

      <p className="c1">
        <span className="c2">
          Pourquoi avons-nous besoin de certaines donn&eacute;es personnelles ?
        </span>
      </p>

      <p className="c1">
        <span className="c0">Docteury</span>
        <span className="c0">
          &nbsp;est un Service qui permet &agrave; tout Utilisateur de trouver
          un Professionnel de Sant&eacute; et de prendre rendez-vous en ligne
          avec l&rsquo;un d&rsquo;entre eux.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          La collecte de certaines informations personnelles est
          n&eacute;cessaire pour organiser la mise en relation de
          l&rsquo;Utilisateur avec un Professionnel de Sant&eacute; et permettre
          &agrave; ce dernier de recevoir son Patient dans les meilleures
          conditions. Par ailleurs, certaines de vos donn&eacute;es personnelles
          sont stock&eacute;es pour vous permettre de g&eacute;rer vos
          rendez-vous, les modifier ou les annuler.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Ainsi, pour pouvoir acc&eacute;der au Service, l&rsquo;Utilisateur
          devra ouvrir un &laquo; Compte personnel &raquo; lors de la
          confirmation de sa demande de rendez-vous. La cr&eacute;ation du
          compte est subordonn&eacute;e au choix d&rsquo;un Nom
          d&rsquo;utilisateur et d&rsquo;un Mot de passe (ci-apr&egrave;s les
          &laquo; Identifiants &raquo;).
        </span>
      </p>

      <p className="c1">
        <span className="c2">
          Quel type d&rsquo;informations recueillons-nous?
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Dans le cadre de la mise en &oelig;uvre du Service,
          l&rsquo;Utilisateur sera amen&eacute; &agrave; renseigner des
          donn&eacute;es personnelles le concernant. Parmi les donn&eacute;es
          &agrave; caract&egrave;re personnel de l&rsquo;Utilisateur que la
          Soci&eacute;t&eacute; collecte aupr&egrave;s de lui peuvent figurer :
        </span>
      </p>

      <ul className="c7 lst-kix_1iwn2yu0lzpc-0 start">
        <li className="c3">
          <span className="c0">Ses nom et pr&eacute;noms</span>
        </li>

        <li className="c3">
          <span className="c0">Son adresse email</span>
        </li>

        <li className="c3">
          <span className="c0">
            Son num&eacute;ro de t&eacute;l&eacute;phone
          </span>
        </li>

        <li className="c3">
          <span className="c0">Sa date de naissance</span>
        </li>

        <li className="c3">
          <span className="c0">Son genre</span>
        </li>

        <li className="c3">
          <span className="c0">Le motif de son rendez-vous</span>
        </li>

        <li className="c3">
          <span className="c0">
            Son adresse IP (adresse virtuelle de son ordinateur)
          </span>
        </li>

        <li className="c3">
          <span className="c0">Son mot de passe</span>
        </li>
      </ul>

      <p className="c1">
        <span className="c0">
          Notre Service peut inclure des enqu&ecirc;tes facultatives,
          destin&eacute;es &agrave; recueillir votre opinion sur votre
          exp&eacute;rience sur notre Site, l&rsquo;utilisation de notre
          Service, ou votre opinion sur les Professionnels de Sant&eacute; que
          vous avez consult&eacute;s.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Nous sommes &eacute;galement susceptibles de r&eacute;pertorier vos
          activit&eacute;s et pr&eacute;f&eacute;rences lorsque vous vous rendez
          sur notre Site (voir la rubrique &quot;Cookies&quot; ci-dessous).
        </span>
      </p>

      <p className="c1">
        <span className="c2">
          A quoi servent les informations que nous recueillons ?
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Les donn&eacute;es personnelles collect&eacute;es visent &agrave;
          permettre la mise en &oelig;uvre du Service de prise de rendez-vous en
          ligne. Ces donn&eacute;es sont n&eacute;cessaires au bon
          fonctionnement du Service et ne sont utilis&eacute;es qu&rsquo;aux
          fins suivantes :
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; Transmettre au Professionnel de Sant&eacute; les informations
          n&eacute;cessaire &agrave; l&rsquo;organisation et la
          pr&eacute;paration du rendez-vous de l&rsquo;Utilisateur
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; Permettre &agrave; l&rsquo;Utilisateur de g&eacute;rer
          &agrave; tout moment l&rsquo;ensemble de ses rendez-vous via un Compte
          personnel s&eacute;curis&eacute;
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; Envoyer &agrave; l&rsquo;Utilisateur des emails
          administratifs, par exemple pour vous pr&eacute;venir de la
          confirmation d&rsquo;un rendez-vous, de sa modification ou de son
          annulation, et vous rappeler les rendez-vous que vous avez
          programm&eacute;s
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; Traiter les demandes re&ccedil;ues par le Service Client
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; Proc&eacute;der &agrave; des analyses et recherches internes
          dans le but d&rsquo;am&eacute;liorer notre Site et notre Service
        </span>
      </p>

      <p className="c1">
        <span className="c2">
          Qui peut acc&eacute;der aux informations que nous recueillons ?
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Certaines des informations personnelles collect&eacute;es pourront
          &ecirc;tre transmises au Professionnel de Sant&eacute; aupr&egrave;s
          duquel vous avez effectu&eacute; une demande de rendez-vous afin de
          lui permettre d&rsquo;organiser le rendez-vous dans les meilleures
          conditions et de pr&eacute;parer sa consultation / son intervention.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Chaque Professionnel de Sant&eacute; destinataire d&rsquo;informations
          personnelles dans le cadre du Service s&rsquo;engage en y
          adh&eacute;rant &agrave; garantir leur confidentialit&eacute;, et ce
          dans le respect du secret professionnel auquel il est soumis.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          La Soci&eacute;t&eacute; s&rsquo;engage &agrave; ne transmettre ces
          donn&eacute;es &agrave; caract&egrave;re personnel &agrave; aucun
          autre tiers non autoris&eacute;.
        </span>
      </p>

      <p className="c1">
        <span className="c2">Cookies</span>
      </p>

      <p className="c1">
        <span className="c0">
          Afin de faciliter l&rsquo;acc&egrave;s au Service, la
          Soci&eacute;t&eacute; utilise des cookies sur son Site. Les Cookies
          sont de petits fichiers texte qu&#39;un site Web envoie &agrave; votre
          ordinateur pour enregistrer votre activit&eacute; en ligne. Les
          cookies permettent aux internautes de naviguer facilement sur le Site
          et d&#39;ex&eacute;cuter divers fonctions. Les cookies visent
          notamment &agrave; am&eacute;liorer la fluidit&eacute; du Service et
          &agrave; permettre le fonctionnement optimal du Site.
          L&rsquo;Utilisateur est inform&eacute; que lorsque ces derniers sont
          d&eacute;sactiv&eacute;s, le Site pourrait ne pas fonctionner de
          mani&egrave;re optimale ou que certaines fonctionnalit&eacute;s
          pourraient ne pas &ecirc;tre accessibles.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          A aucun moment, ces cookies ne permettent &agrave; la
          Soci&eacute;t&eacute; d&#39;identifier personnellement
          l&#39;Utilisateur. La dur&eacute;e de conservation de ces cookies dans
          l&#39;ordinateur de l&#39;Utilisateur n&#39;exc&egrave;de pas un (1)
          mois. L&#39;Utilisateur est cependant inform&eacute; qu&#39;il a la
          facult&eacute; de s&#39;opposer &agrave; l&#39;enregistrement de ces
          cookies et ce notamment en configurant son navigateur Internet pour ce
          faire.
        </span>
      </p>

      <p className="c1">
        <span className="c2">S&eacute;curit&eacute;</span>
      </p>

      <p className="c1">
        <span className="c0">
          Nous prenons les mesures requises pour prot&eacute;ger vos
          informations personnelles. Celles-ci comprennent la mise en place de
          processus et proc&eacute;dures visant &agrave; minimiser
          l&#39;acc&egrave;s non autoris&eacute; &agrave; vos informations ou
          leur divulgation et &agrave; leur h&eacute;bergement sur des serveurs
          s&eacute;curis&eacute;s.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Nous ne pouvons cependant pas garantir l&#39;&eacute;limination totale
          du risque d&#39;usage abusif de vos informations personnelles par des
          intrus. Prot&eacute;gez les mots de passe de vos comptes et ne les
          communiquez &agrave; personne. Vous devez nous contacter
          imm&eacute;diatement si vous d&eacute;couvrez une utilisation non
          autoris&eacute;e de votre mot de passe ou toute autre violation de
          s&eacute;curit&eacute;.
        </span>
      </p>

      <p className="c1">
        <span className="c2">Liens vers des sites tiers</span>
      </p>

      <p className="c1">
        <span className="c0">
          Notre Site peut contenir des liens vers d&#39;autres sites tiers (par
          exemple des sites personnels de Professionnels de Sant&eacute;).
          Cependant, une fois que vous avez utilis&eacute; ces liens pour
          quitter notre site, vous devez noter que nous n&#39;avons aucun
          contr&ocirc;le sur ces sites Internet tiers. Par cons&eacute;quent,
          nous ne pouvons pas &ecirc;tre tenu responsables de la protection et
          la confidentialit&eacute; des informations que vous fournissez en
          visitant ces sites car ces sites ne sont pas r&eacute;gis par la
          pr&eacute;sente Politique de Confidentialit&eacute;. Vous devez faire
          preuve de prudence et de lire la d&eacute;claration de
          confidentialit&eacute; applicable au site en question.
        </span>
      </p>

      <p className="c1">
        <span className="c2">D&eacute;claration &agrave; la CNIL</span>
      </p>

      <p className="c1">
        <span className="c0">
          Conform&eacute;ment aux dispositions de la Loi n&deg;78-17 du 6
          janvier 1978 relative &agrave; l&#39;Informatique, aux fichiers et aux
          Libert&eacute;s, modifi&eacute;e par la Loi n&deg;2004-81 du 6
          ao&ucirc;t 2004, l&#39;Utilisateur est inform&eacute; que :
        </span>
      </p>

      <ul className="c7 lst-kix_wklkp66f660n-0 start">
        <li className="c3">
          <span className="c0">
            le Site Internet a fait l&#39;objet d&#39;une d&eacute;claration
            CNIL n&deg;1672087
          </span>
        </li>

        <li className="c3">
          <span className="c0">
            les fichiers clients de la Soci&eacute;t&eacute; ont
            &eacute;galement fait l&#39;objet d&#39;une d&eacute;claration
            aupr&egrave;s de la Commission Nationale de l&#39;Informatique et
            des Libert&eacute;s (CNIL)
          </span>
        </li>
      </ul>

      <p className="c1">
        <span className="c0">
          La Soci&eacute;t&eacute; s&#39;engage &agrave; prot&eacute;ger
          l&#39;ensemble des donn&eacute;es &agrave; caract&egrave;re personnel
          de l&#39;Utilisateur, lesquelles donn&eacute;es sont recueillies et
          trait&eacute;es par la Soci&eacute;t&eacute; avec la plus stricte
          confidentialit&eacute;, conform&eacute;ment aux dispositions de la Loi
          pr&eacute;cit&eacute;e.
        </span>
      </p>

      <p className="c1">
        <span className="c2">
          Droit d&rsquo;opposition d&rsquo;acc&egrave;s, de rectification et de
          suppression de l&rsquo;Utilisateur
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Conform&eacute;ment aux dispositions des articles 38, 39 et 40 de la
          Loi n&deg;78-17 relative &agrave; l&#39;informatique, aux fichiers et
          aux libert&eacute;s du 6 janvier 1978 modifi&eacute;e par la Loi
          n&deg;2004-81 du 6 ao&ucirc;t 2004, l&#39;Utilisateur a, &agrave; tout
          moment, la facult&eacute; de :
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; S&#39;opposer au traitement de ses donn&eacute;es &agrave;
          caract&egrave;re personnel dans le cadre des Services fournis par la
          Soci&eacute;t&eacute; (article 38 de Loi Informatique et
          Libert&eacute;s),
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; S&#39;opposer &agrave; la communication des donn&eacute;es
          &agrave; caract&egrave;re personnel le concernant &agrave; des tiers
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; Acc&eacute;der &agrave; l&#39;ensemble de ses donn&eacute;es
          &agrave; caract&egrave;re personnel trait&eacute;es dans le cadre du
          Service fourni par la Soci&eacute;t&eacute; (article 39 de Loi
          Informatique et Libert&eacute;s),
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          &middot; Rectifier, mettre &agrave; jour et supprimer ses
          donn&eacute;es &agrave; caract&egrave;re personnel trait&eacute;es
          dans le cadre des Services fournis par la Soci&eacute;t&eacute;
          (article 40 de Loi Informatique et Libert&eacute;s).
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Pour exercer ses droits, et conform&eacute;ment aux alin&eacute;as 1er
          des articles 39 et 40 de la Loi n&deg;78-17 relative &agrave;
          l&#39;informatique, aux fichiers et aux libert&eacute;s du 6 janvier
          1978 modifi&eacute;e par la Loi n&deg;2004-81 du 6 ao&ucirc;t 2004, il
          suffit &agrave; l&rsquo;Utilisateur d&rsquo;adresser un courrier
          simple en mentionnant son identit&eacute; (nom, pr&eacute;nom, adresse
          email et copie de sa pi&egrave;ce d&rsquo;identit&eacute;) &agrave; la
          Soci&eacute;t&eacute; (voir la section &laquo; Contacts &raquo;
          ci-apr&egrave;s).
        </span>
      </p>

      <p className="c1">
        <span className="c2">Contacts</span>
      </p>

      <p className="c1">
        <span className="c0">
          Vous pouvez nous contacter pour exercer vos droits
          d&rsquo;acc&egrave;s, de rectification et de suppression de vos
          donn&eacute;es personnelles, ou pour toute question concernant notre
          Politique de Confidentialit&eacute;.
        </span>
      </p>

      <p className="c1">
        <span className="c0">
          Pour cela, vous pouvez envoyer un mail &agrave;
        </span>{" "}
        <span className="c9">contact@docteury.tn</span>
        <span className="c0">..</span>
      </p>

      <p className="c1">
        <span className="c0">
          Pour plus d&rsquo;informations vous pouvez nous contacter au +216 53
          226 650.
        </span>
      </p>

      <p className="c6">
        <span></span>
      </p>
    </div>
  );
};

export default CONFIDENTIALITE;
