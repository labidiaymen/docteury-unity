const scraperObject = {
  async scraper(browser, url) {
    let page = await browser.newPage();
    let city, textContent;
    try {
      await page.setViewport({ width: 1366, height: 768 });

      // await page.setDefaultNavigationTimeout(120000);

      console.log(`Navigating to ${url}...`);
      // Navigate to the selected page
      await page.goto(url);
      // Wait for the required DOM to be rendered
      await page.waitForSelector(".list__label--spee");
      textContent = await page.evaluate(() => {
        return document.querySelector(".list__label--spee").textContent;
      });
      const rdvHref = await page.evaluate(() => {
        return document.querySelector(".button__rdv").href;
      });

      const splitedURL = rdvHref.split("/");
      city = splitedURL[5];
      console.log(rdvHref);
      await new Promise((resolve) => setTimeout(resolve, 5000));
    } catch (e) {
    } finally {
      await page.close();
    }
    return {
      spec: textContent,
      city,
    };
  },
};

module.exports = scraperObject;
