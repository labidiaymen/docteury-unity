import nc from "next-connect";
import { all } from "@/middlewares/index";
import { getDoctors, insertDoctor } from "@/db/index";
import prisma from "../../../lib/prisma";
prisma.doc;
const handler = nc();
handler.use(all);

const maxAge = 1 * 24 * 60 * 60;

handler.get(async (req, res) => {
  try {
    // const doctors = await getDoctors(
    //   req.db
    //   // req.query.from ? new Date(req.query.from) : undefined,
    //   // req.query.by,
    //   // req.query.limit ? parseInt(req.query.limit, 10) : undefined,
    // );
    // const feed = await insertDoctor();

    if (req.query.from && doctors.length > 0) {
      // This is safe to cache because from defines
      //  a concrete range of doctors
      // res.setHeader('cache-control', `public, max-age=${maxAge}`);
    }

    res.send({ feed: [] });
  } catch (e) {
    console.log(e);
  }
});

handler.post(async (req, res) => {
  // if (!req.user) {
  //   return res.status(401).send("unauthenticated");
  // }

  // if (!req.body.content)
  //   return res.status(400).send("You must write something");
  let doctor;
  try {
    const doctor = await insertDoctor(req.body);
  } catch (e) {
    console.log( req.body.specialities.connect.slug);
  }

  return res.json({ doctor });
});

export default handler;
