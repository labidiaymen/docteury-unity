import nc from "next-connect";
import { all } from "@/middlewares/index";
import { getDoctors, insertDoctor, updateSingleDoctor } from "@/db/index";
import prisma from "../../../lib/prisma";
prisma.doc;
const handler = nc();
handler.use(all);

handler.post(async (req, res) => {
  const {
    id,
    email,
    name,
    lastname,
    speciality,
    city,
    bio,
    cabin_number,
    cabin_phone,
    lat,
    long,
    street,
    cnam,
    paiement_bankcard,
    paiement_cash,
    paiement_cheque,
  } = req.body;

  let result;
  try {
    let medecinToHandle;
    if (!id) {
      medecinToHandle = {
        email,
        name,
        lastname,
        cnam,
        bio,
        paiement_bankcard,
        paiement_cash,
        paiement_cheque,
        specialities: {
          connect: {
            slug: speciality,
          },
        },
        City: {
          connect: {
            slug: city,
          },
        },
        address: {
          create: {
            street: street,
            cabin_phone: {
              create: {
                number: cabin_phone,
              },
            },
            long: String(long),
            lat: String(lat),
          },
        },
      };
      result = await insertDoctor(medecinToHandle);
    } else {
      const where = {
        id,
      };
      medecinToHandle = {
        name,
        lastname,
      };
      result = await updateSingleDoctor(where, medecinToHandle);
    }
  } catch (e) {
    console.log(e);
  }

  return res.json({ result });
});

export default handler;
