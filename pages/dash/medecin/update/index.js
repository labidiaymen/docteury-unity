import { useForm } from "react-hook-form";
import Select from "react-select";
import { useContext, useEffect, useState } from "react";
import { useSession, signIn, signOut } from "next-auth/client";

import styles from "./update.module.css";
import { AppContext } from "@/components/common/AppProvider";
import { string_to_slug } from "@/lib/slugify";
import { getDoctor } from "@/db/doctor";
const DashUpdateMedecin = ({ medecin }) => {
  const {
    register,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = useForm();
  const [cities, setCities, specialities, setSpecialities] =
    useContext(AppContext);
  const [currentSpeciality, setCurrentSpeciality] = useState();
  const [currentMedecin, setCurrentMedecin] = useState();
  const [currentCity, setCurrentCity] = useState();
  let [position, setPosition] = useState({});
  let [otherOption, setOtherOption] = useState(false);
  const cities_options = cities.map((city) => ({
    value: city.slug,
    label: city.name,
  }));
  const specialities_options = (specialities || []).map((speciality) => ({
    value: speciality.slug,
    label: speciality.name,
  }));

  const onSpecChange = ({ value }) => {
    setCurrentSpeciality(value);
  };

  const onCityChange = ({ value }) => {
    setCurrentCity(value);
  };

  useEffect(() => {
    fetch("/api/common")
      .then((result) => result.json())
      .then(({ cities, specialities }) => {
        setCities(cities);
        setSpecialities(specialities);
      });
  }, []);
  useEffect(() => {
    if (medecin) {
      setCurrentMedecin(medecin);
      const {
        name,
        lastname,
        email,
        citySlug,
        specialities: [speciality],
        bio,
        address: {
          long,
          lat,
          street,
          cabin_phone: [phone1],
        },
      } = medecin;
      setValue("name", name, { shouldValidate: true });
      setValue("lastname", lastname, { shouldValidate: true });
      setValue("email", email, { shouldValidate: true });
      setValue("street", street, { shouldValidate: true });
      setValue("cabin_phone", phone1.number, { shouldValidate: true });
      setValue("bio", bio, { shouldValidate: true });
      setCurrentCity(citySlug);
      speciality && setCurrentSpeciality(speciality.slug);
      setValue("lat", lat, { shouldValidate: true });
      setValue("long", long, { shouldValidate: true });
    }
  }, []);

  const setCurrentDevicePosition = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function (position) {
        const currentPosition = {
          lat: position.coords.latitude,
          long: position.coords.longitude,
        };
        setValue("lat", currentPosition.lat, { shouldValidate: true });
        setValue("long", currentPosition.long, { shouldValidate: true });
      });
    }
  };
  const [session, loading] = useSession();
  if (typeof window !== "undefined" && loading) return null;

  const onSubmit = async (data) => {
    const { email, name, lastname } = data;
    if (!email) {
      data.email = string_to_slug(` Dr. ${name} ${lastname}`) + "@docteury.tn";
    }
    if (currentCity && currentSpeciality) {
      data = {
        ...data,
        speciality: currentSpeciality,
        city: currentCity,
      };
    }
    if (currentMedecin) {
      data.id = currentMedecin.id;
    }

    const res = await fetch("/api/medecin/create", {
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    });
  };
  return (
    <>
      {session && (
        <div class="container">
          <button onClick={() => signOut()}>Sign out</button>
          <h1>Ajouter un medecin</h1>
          <div className="grid mt-2" className={styles.mt_2}>
            <form className="form-group " onSubmit={handleSubmit(onSubmit)}>
              <div className="row">
                <div className="col-md-5 col-sm-12">
                  <div className="form-group">
                    <input
                      className="form-control"
                      placeholder="Nom"
                      {...register("name", { required: true })}
                    />
                    {errors.name && <span>This field is required</span>}
                  </div>
                  <div className="form-group">
                    <input
                      className="form-control"
                      placeholder="Prénom"
                      {...register("lastname", { required: true })}
                    />
                    {errors.lastname && <span>This field is required</span>}
                  </div>
                  <div className="form-group">
                    <input
                      className="form-control"
                      placeholder="Email"
                      type="email"
                      {...register("email", { required: false })}
                    />
                  </div>

                  <div className="form-group">
                    <textarea
                      placeholder="Bio"
                      className="form-control"
                      {...register("bio", { required: false })}
                    />
                  </div>

                  <div class="form-group">
                    <Select
                      instanceId="postType0"
                      value={specialities_options.filter(
                        ({ value }) => value === currentSpeciality
                      )}
                      className="search_select_input"
                      placeholder={"Spécialité"}
                      options={specialities_options}
                      value={specialities_options.filter(
                        ({ value }) => value === currentSpeciality
                      )}
                      onChange={onSpecChange}
                    />
                    {errors.speciality && <span>This field is required</span>}
                  </div>
                  <div class="form-group">
                    <Select
                      instanceId="postType"
                      value={cities_options.filter(
                        ({ value }) => value === currentCity
                      )}
                      className="search_select_input"
                      placeholder={"Ville"}
                      options={cities_options}
                      value={cities_options.filter(
                        ({ value }) => value === currentCity
                      )}
                      onChange={onCityChange}
                    />
                  </div>
                  {otherOption && (
                    <div className="form-group">
                      <div className="form-group">
                        <label>Paiement cheque</label>{" "}
                        <input
                          name="paiement_cheque"
                          type="checkbox"
                          {...register("paiement_cheque")}
                          id="paiement_cheque"
                        />
                      </div>
                      <div className="form-group">
                        <label>Cnam</label>{" "}
                        <input
                          name="cnam"
                          type="checkbox"
                          {...register("cnam")}
                          id="cnam"
                        />
                      </div>
                      <div className="form-group">
                        <label>Paiement bc</label>{" "}
                        <input
                          name="paiement_bankcard"
                          type="checkbox"
                          {...register("paiement_bankcard")}
                          id="paiement_bankcard"
                        />
                      </div>
                      <div className="input-group-text">
                        <label>Cash</label>{" "}
                        <input
                          name="paiement_cash"
                          type="checkbox"
                          className="form-check-input"
                          {...register("paiement_cash")}
                          id="paiement_cash"
                        />
                      </div>
                    </div>
                  )}
                  {!otherOption && (
                    <a
                      className="btn btn-success"
                      onClick={() => setOtherOption(true)}
                    >
                      Show all option
                    </a>
                  )}
                </div>
                <div className="col-md-5">
                  <div>
                    <h3>Address</h3>
                    <div className="form-group">
                      <input
                        className="form-control"
                        placeholder="Rue"
                        {...register("street", { required: true })}
                      />
                      {errors.street && <span>This field is required</span>}
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control"
                        placeholder="Num cabinet"
                        {...register("cabin_number", { required: false })}
                      />
                      {errors.street && <span>This field is required</span>}
                    </div>

                    <div className="form-group">
                      <input
                        className="form-control"
                        placeholder="Tél cabinet"
                        {...register("cabin_phone", { required: true })}
                      />
                      {errors.cabin_phone && (
                        <span>This field is required</span>
                      )}
                    </div>

                    <div className="form-group">
                      <input
                        defaultValue={position.long}
                        className="form-control"
                        placeholder="Long"
                        {...register("long", { required: false })}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        defaultValue={position.lat}
                        className="form-control"
                        placeholder="Lat"
                        {...register("lat", { required: false })}
                      />
                    </div>
                    <div className="form-group">
                      <a
                        className="btn btn-success"
                        onClick={setCurrentDevicePosition}
                      >
                        Set current device position
                      </a>
                    </div>
                  </div>
                </div>
              </div>{" "}
              <div className="row">
                <div className="col-md-5">
                  <input
                    style={{ marginTop: "20px" }}
                    className="btn btn-primary"
                    type="submit"
                    value={
                      currentMedecin?.id
                        ? "Mise à jour du Medecin"
                        : "Créer le Medecin"
                    }
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
      )}
      {!session && (
        <div class="container">
          <h1>Protected Page</h1>
          <p>You can't view this page because you are not signed in.</p>
          <button onClick={() => signIn()}>Sign in</button>
        </div>
      )}
    </>
  );
};

export default DashUpdateMedecin;

export async function getServerSideProps(ctx) {
  const { id } = ctx.query;
  let medecin;
  if (id) {
    medecin = await getDoctor({
      query: {
        id,
      },
    });
  }

  return {
    props: { medecin },
  };
}
