const CGU = function () {
  return (
    <div class="container">
      <div class="col-sm-8 col-sm-offset-1">
        <h2 class="c9">
          Conditions G&eacute;n&eacute;rales d&rsquo;Utilisation du Service
        </h2>

        <p class="c3 c8"></p>
        <p class="c3">
          <span class="c4">1.1. D&eacute;finitions</span>
        </p>
        <p class="c3">
          <span class="c0">
            Pour les besoins des pr&eacute;sentes Conditions
            G&eacute;n&eacute;rales d&rsquo;Utilisation, les termes avec
            commen&ccedil;ant par une majuscule sont d&eacute;finis comme suit :
          </span>
        </p>
        <ul class="c7 lst-kix_bmnc2k4z0a4h-0 start">
          <li class="c1">
            <span class="c0">Le terme </span>
            <span class="c4">&laquo; Service &raquo;</span>
            <span class="c0">
              &nbsp;d&eacute;signe ici l&rsquo;ensemble des services
              propos&eacute;s par la soci&eacute;t&eacute; Docteury via le Site
              Internet (ou tout autre moyen de communication) tels que
              d&eacute;finis dans la rubrique 2. Description du Service.
            </span>
          </li>
          <li class="c1">
            <span class="c0">Le terme </span>
            <span class="c4">&laquo; Site Internet &raquo;</span>
            <span class="c0">&nbsp;d&eacute;signe le site </span>
            <span class="c2">
              <a
                class="c6"
                href="https://www.google.com/url?q=http://www.docteury.tn/&amp;sa=D&amp;usg=AFQjCNFvrsNIhtozEvdH6wGJuF_-7I8QMg"
              >
                www.docteury.tn
              </a>
            </span>
            <span class="c0">
              &nbsp;&eacute;dit&eacute; et exploit&eacute; par la
              soci&eacute;t&eacute; Docteury et permettant
              d&rsquo;acc&eacute;der aux Services.
            </span>
          </li>
          <li class="c1">
            <span class="c0">Les termes </span>
            <span class="c4">
              &laquo; Professionnel de Sant&eacute; &raquo;
            </span>
            <span class="c0">&nbsp;ou </span>
            <span class="c4">&laquo; Praticien &raquo;</span>
            <span class="c0">
              &nbsp;d&eacute;signent ici les professionnels de sant&eacute;
              ayant souscrit un contrat d&rsquo;adh&eacute;sion au Service
              permettant la mise &agrave; jour de leurs disponibilit&eacute;s et
              la prise de rendez-vous &agrave; distance.
            </span>
          </li>
          <li class="c1">
            <span class="c0">Le terme </span>
            <span class="c4">&laquo; Prestation &raquo;</span>
            <span class="c0">
              &nbsp;d&eacute;signe ici toute consultation, acte m&eacute;dical,
              ou soin propos&eacute; et r&eacute;alis&eacute; par un
              Professionnel de Sant&eacute; dans le cadre d&rsquo;un
              rendez-vous.
            </span>
          </li>
          <li class="c1">
            <span class="c0">Les termes </span>
            <span class="c4">&laquo; Vous &raquo;</span>
            <span class="c0">, </span>
            <span class="c4">&laquo; Utilisateur &raquo; </span>
            <span class="c0">ou</span>
            <span class="c4">&nbsp;&laquo; Patient &raquo;</span>
            <span class="c0">
              &nbsp;d&eacute;signent ici toute personne physique qui
              acc&egrave;de au Site Internet et b&eacute;n&eacute;ficie du
              Service dans le cadre d&rsquo;un usage strictement priv&eacute;.
            </span>
          </li>
          <li class="c1">
            <span class="c0">Le terme </span>
            <span class="c4">&laquo; Soci&eacute;t&eacute; &raquo;</span>
            <span class="c0">
              &nbsp;d&eacute;signe la soci&eacute;t&eacute; Docteury qui
              &eacute;dite et exploite le Site Internet et fournit le Service
              aux Utilisateurs et aux Professionnels de Sant&eacute;.
            </span>
          </li>
        </ul>
        <p class="c3">
          <span class="c4">
            1.3. Acceptation des Conditions d&rsquo;Utilisation du Service
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Les pr&eacute;sentes Conditions d&rsquo;Utilisation du Service ont
            pour objet de d&eacute;finir les conditions dans lesquels
            l&rsquo;Utilisateur peut b&eacute;n&eacute;ficier du Service fourni
            par la Soci&eacute;t&eacute;. L&rsquo;utilisation de ce Service est
            soumise &agrave; l&rsquo;acception inconditionnelle par
            l&rsquo;Utilisateur des Conditions G&eacute;n&eacute;rales
            d&rsquo;Utilisation du Service.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            En acc&eacute;dant au Site Internet et en b&eacute;n&eacute;ficiant
            du Service, l&rsquo;Utilisateur reconna&icirc;t donc avoir lu et
            compris l&rsquo;int&eacute;gralit&eacute; des pr&eacute;sentes
            Conditions G&eacute;n&eacute;rales d&rsquo;Utilisation, et les
            accepter sans restrictions ni r&eacute;serves.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Si l&rsquo;Utilisateur n&rsquo;accepte pas ces conditions, il doit
            cesser d&rsquo;utiliser le Site Internet et renoncer &agrave;
            b&eacute;n&eacute;ficier du Service propos&eacute; par la
            Soci&eacute;t&eacute;.
          </span>
        </p>
        <p class="c3">
          <span class="c4">1.4. Champs d&rsquo;application</span>
        </p>
        <p class="c3">
          <span class="c0">
            Les pr&eacute;sentes Conditions G&eacute;n&eacute;rales
            d&rsquo;Utilisation sont en vigueur &agrave; compter du 25 avril
            2013.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Elles sont applicables &agrave; toute utilisation du Site par
            l&rsquo;Utilisateur. Les pr&eacute;sentes conditions sont soumises
            au droit fran&ccedil;ais : tout Utilisateur &eacute;tranger accepte
            express&eacute;ment l&rsquo;application de la loi fran&ccedil;aise
            en utilisant le Service.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Dans l&rsquo;hypoth&egrave;se o&ugrave; une clause contractuelle
            particuli&egrave;re serait nulle, ill&eacute;gale, ou inapplicable,
            la validit&eacute; des autres dispositions des Conditions
            G&eacute;n&eacute;rales n&rsquo;en serait aucunement
            affect&eacute;e.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            1.5. Modification des Conditions d&rsquo;Utilisation du Service
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            La Soci&eacute;t&eacute; se r&eacute;serve la facult&eacute; de
            modifier les pr&eacute;sentes Conditions G&eacute;n&eacute;rales
            d&rsquo;Utilisation du Service &agrave; tout moment et sans
            pr&eacute;avis ni information, et sans pr&eacute;judice. Les
            Conditions d&rsquo;Utilisation du Service applicables sont celles en
            vigueur &agrave; la date et &agrave; l&rsquo;heure
            d&rsquo;activation par l&rsquo;Utilisateur du bouton &laquo;
            J&rsquo;accepte les Conditions G&eacute;n&eacute;rale
            d&rsquo;Utilisation &raquo; lors de l&rsquo;adh&eacute;sion au
            Service.
          </span>
        </p>
        <p class="c3">
          <span class="c4">2. Description du Service</span>
        </p>
        <p class="c3">
          <span class="c4">2.1. Objectif du Service</span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Site internet www.docteury.tn permet de rechercher un
            Professionnel de Sant&eacute;, de visualiser les cr&eacute;neaux
            disponibles chez les Professionnels de Sant&eacute; autour
            d&rsquo;une adresse, et de prendre rendez-vous en ligne avec
            l&rsquo;un d&rsquo;entre eux.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Service vise &agrave; faciliter la mise en relation entre le
            Patient et son Praticien et en aucun &agrave; la remplacer. Par
            ailleurs, le Service ne constitue pas un service de recommandation
            d&rsquo;un Professionnel de Sant&eacute;, et ne vise pas &agrave; se
            substituer au libre choix de son Praticien, conform&eacute;ment
            &agrave; la l&eacute;gislation en vigueur.
          </span>
        </p>
        <p class="c3">
          <span class="c4">2.2. Fonctionnement du Service</span>
        </p>
        <p class="c3">
          <span class="c0">
            L&rsquo;Utilisateur acc&egrave;de au Service via le Site{" "}
          </span>
          <span class="c2">
            <a
              class="c6"
              href="https://www.google.com/url?q=http://www.docteury.tn&amp;sa=D&amp;usg=AFQjCNFdYO_0fm_2X5Vyz8ABK1uiQAI-CQ"
            >
              www.docteury.tn
            </a>
          </span>
          <span class="c0">
            &nbsp;ou via d&rsquo;autres services g&eacute;r&eacute;s par la
            Soci&eacute;t&eacute; (applications mobiles).
          </span>
        </p>
        <p class="c3">
          <span class="c0">Sur le site, l&rsquo;Utilisateur peut :</span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; Trouver un Professionnel de Sant&eacute; &agrave; partir de
            param&egrave;tres g&eacute;ographiques, de sp&eacute;cialit&eacute;
            exerc&eacute;e ou nominatifs
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; Consulter les informations sur les Professionnels de
            Sant&eacute; correspondant &agrave; leur recherche que ces derniers
            ont choisi de proposer sur leur fiche praticien
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; De prendre rendez-vous avec un Professionnel de
            Sant&eacute; de leur choix selon les disponibilit&eacute;s de ces
            derniers
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; De g&eacute;rer ses rendez-vous (annulation, modification
            des rendez-vous) et d&rsquo;en suivre l&rsquo;historique sur un
            compte personnel
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Chaque demande de rendez-vous est transmise en temps r&eacute;el au
            Professionnel de Sant&eacute; qui doit l&rsquo;accepter pour que le
            rendez-vous soit confirm&eacute; et que la mise en relation entre
            l&rsquo;Utilisation et le Professionnel de Sant&eacute; soit
            r&eacute;put&eacute;e av&eacute;r&eacute;e.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            2.3. Gratuit&eacute; du Service pour l&rsquo;Utilisateur
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Service de recherche d&rsquo;un Professionnel de Sant&eacute; et
            de prise de rendez-vous en ligne est propos&eacute; gratuitement
            &agrave; l&rsquo;Utilisateur.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            2.4. Cr&eacute;ation d&rsquo;un Compte personnel
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Pour pouvoir acc&eacute;der au Service, et afin d&rsquo;en assurer
            son bon fonctionnement, l&rsquo;Utilisateur devra ouvrir un &laquo;
            Compte personnel &raquo; lors de la confirmation de sa demande de
            rendez-vous. La cr&eacute;ation du compte est subordonn&eacute;e au
            choix d&rsquo;un Nom d&rsquo;utilisateur et d&rsquo;un Mot de passe
            (ci-apr&egrave;s les &laquo; Identifiants &raquo;).
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            L&#39;Utilisateur est seul et enti&egrave;rement responsable de
            l&#39;utilisation des Identifiants le concernant et s&#39;engage
            &agrave; mettre tout en &oelig;uvre pour conserver secret ses
            Identifiants et &agrave; ne pas les divulguer, &agrave; qui que ce
            soit, sous quelque forme que ce soit.
          </span>
        </p>
        <p class="c3">
          <span class="c4">2.5. Limitations du Service</span>
        </p>
        <p class="c3">
          <span class="c0">
            Le r&eacute;f&eacute;rencement sur le Site est ouvert gratuitement
            &agrave; l&rsquo;ensemble des Professionnels de Sant&eacute; qui en
            font la demande. Ainsi, seuls les Professionnels de Sant&eacute; qui
            auront engag&eacute; une d&eacute;marche proactive pour &ecirc;tre
            r&eacute;f&eacute;renc&eacute;s sur le Site ou adh&eacute;rer au
            Service pourront &ecirc;tre identifi&eacute;s au cours d&rsquo;une
            recherche. Le Service ne permet donc en aucune fa&ccedil;on &agrave;
            l&rsquo;Utilisateur d&rsquo;identifier de mani&egrave;re exhaustive
            l&rsquo;ensemble des Professionnels de Sant&eacute;.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Service de recherche d&rsquo;un Professionnel de Sant&eacute; et
            de prise de rendez-vous en ligne est propos&eacute; gratuitement
            &agrave; l&rsquo;Utilisateur. Il est en revanche entendu que les
            actes ou soins m&eacute;dicaux pratiqu&eacute;s par le Professionnel
            de Sant&eacute; dans le cadre d&rsquo;un rendez-vous pris via le
            Site pourront faire l&rsquo;objet d&rsquo;une facturation
            d&rsquo;honoraires, conform&eacute;ment aux conventions de soins en
            vigueur chez ce Professionnel de Sant&eacute;.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            La mise en &oelig;uvre de la Prestation par le Professionnel de
            Sant&eacute; est sous l&rsquo;enti&egrave;re responsabilit&eacute;
            du Professionnel de Sant&eacute;. La Soci&eacute;t&eacute; ne pourra
            &ecirc;tre tenue responsable de la non-ex&eacute;cution ou de la
            mauvaise ex&eacute;cution de la Prestation propos&eacute;e par le
            Professionnel de Sant&eacute; et ne pourra &ecirc;tre redevable des
            frais &eacute;ventuels engag&eacute;s par l&rsquo;Utilisateur dans
            ce cadre. L&rsquo;utilisation du Service par le Professionnel de
            Sant&eacute; n&rsquo;exempt ni n&rsquo;att&eacute;nue en aucune
            fa&ccedil;on celui-ci de ses obligations professionnelles et de sa
            responsabilit&eacute; vis-&agrave;-vis de l&rsquo;Utilisateur.
          </span>
        </p>
        <p class="c3">
          <span class="c4">2.6. Situations d&rsquo;urgence</span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Service propos&eacute; par Docteury n&rsquo;est pas un service
            d&rsquo;urgence. Chaque demande de rendez-vous formul&eacute;e par
            l&rsquo;Utilisateur est soumise &agrave; l&rsquo;acceptation du
            praticien. La Soci&eacute;t&eacute; ne pourra pas &ecirc;tre tenue
            pour responsable de l&rsquo;indisponibilit&eacute; d&rsquo;un
            Professionnel de Sant&eacute;, du d&eacute;lai d&rsquo;acceptation
            du rendez-vous, ou de sa modification ou son annulation
            &eacute;ventuelle. En cons&eacute;quence, en situation
            d&rsquo;urgence, l&rsquo;Utilisateur ne doit pas utiliser le Service
            et doit s&rsquo;adresser directement aux services d&rsquo;urgences
            situ&eacute;s &agrave; proximit&eacute; de lui ou en appeler le 15.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            3. Informations fournies par les Professionnels de Sant&eacute;
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            3.1. Informations sur les Professionnels de Sant&eacute;
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Service est propos&eacute; par la Soci&eacute;t&eacute; &agrave;
            la demande du Professionnel de Sant&eacute;. Dans le cadre de son
            adh&eacute;sion au Service, la Soci&eacute;t&eacute; met &agrave;
            disposition du Professionnel de Sant&eacute; un page sur laquelle ce
            dernier peut renseigner des informations le concernant ou concernant
            son exercice m&eacute;dical (dipl&ocirc;mes, honoraires
            etc&hellip;). Ces informations sont fournies par les Professionnels
            de Sant&eacute; : elles ne sont pas v&eacute;rifi&eacute;es ni
            valid&eacute;es par la Soci&eacute;t&eacute; et sont sous la seule
            responsabilit&eacute; du Professionnel de Sant&eacute;.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            3.2. Respect du code d&eacute;ontologique sur Internet
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Les informations fournies par les Professionnels de Sant&eacute;
            visent &agrave; am&eacute;liorer l&rsquo;information de
            l&rsquo;Utilisateur et &agrave; permettre la prise de rendez-vous en
            ligne. Lors de son adh&eacute;sion au Service, chaque Professionnel
            de Sant&eacute; s&rsquo;est engag&eacute; &agrave; respecter
            strictement la Charte Ordinale applicable aux sites internet
            professionnels de Professionnels de Sant&eacute; des
            Chirurgiens-Dentistes et du Code de d&eacute;ontologie sur Internet.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            4. R&egrave;gles d&rsquo;utilisation du service
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            4.1. Conditions d&rsquo;acc&egrave;s au service
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Service est ouvert &agrave; tous mais son utilisation est
            subordonn&eacute;e aux conditions suivantes :
          </span>
        </p>
        <ul class="c7 lst-kix_e31cxqoae92s-0 start">
          <li class="c1">
            <span class="c0">Etre majeur</span>
          </li>
          <li class="c1">
            <span class="c0">
              Reconnaitre avoir lu et compris l&rsquo;int&eacute;gralit&eacute;
              des pr&eacute;sentes Conditions G&eacute;n&eacute;rales
              d&rsquo;Utilisation, et les accepter sans restrictions ni
              r&eacute;serves
            </span>
          </li>
          <li class="c1">
            <span class="c0">
              Consentir &agrave; la collecte et au traitement de donn&eacute;es
              personnelles afin d&rsquo;assurer le bon fonctionnement du
              Service, et ce dans le strict respect de notre{" "}
            </span>
            <span class="c0 c10">
              Politique de Confidentialit&eacute; des donn&eacute;es{" "}
            </span>
            <span class="c0">
              et des r&egrave;gles de protection des donn&eacute;es personnelles
              en vigueur
            </span>
          </li>
          <li class="c1">
            <span class="c0">
              Cr&eacute;er un compte personnel et fournir les informations
              n&eacute;cessaires au bon fonctionnement du Service de prise de
              rendez-vous
            </span>
          </li>
          <li class="c11">
            <span class="c0">
              &middot; Acc&eacute;der au Service en utilisant votre
              v&eacute;ritable identit&eacute; et s&rsquo;engager &agrave;
              renseigner des informations authentiques lors de la
              cr&eacute;ation du compte Utilisateur
            </span>
          </li>
          <li class="c1">
            <span class="c0">
              Cr&eacute;er un compte personnel et fournir les informations
              n&eacute;cessaires au bon fonctionnement du Service de prise de
              rendez-vous
            </span>
          </li>
          <li class="c1">
            <span class="c0">
              Acc&eacute;der au Service en utilisant votre v&eacute;ritable
              identit&eacute; et s&rsquo;engager &agrave; renseigner des
              informations authentiques lors de la cr&eacute;ation du compte
              Utilisateur
            </span>
          </li>
          <li class="c1">
            <span class="c0">
              En aucun cas la Soci&eacute;t&eacute; ne peut &ecirc;tre tenue
              pour responsable de l&rsquo;utilisation frauduleuse du compte
              d&rsquo;un Utilisateur si ce dernier a inform&eacute; directement
              ou indirectement, volontairement ou involontairement un tiers de
              son mot de passe.
            </span>
          </li>
        </ul>
        <p class="c3">
          <span class="c4">
            4.2. R&egrave;gles de bonne conduite sur le site
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            L&rsquo;acc&egrave;s au Service propos&eacute; par la
            Soci&eacute;t&eacute; implique que l&rsquo;Utilisateur
            s&rsquo;engage au respect d&rsquo;une charte de bonne conduite.
            L&rsquo;utilisation du Service est soumise &agrave;
            l&rsquo;acceptation et au strict respect des r&egrave;gles
            ci-dessous :
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; Il est rappel&eacute; &agrave; l&rsquo;Utilisateur que
            l&rsquo;utilisation du Service pour prendre rendez-vous constitue un
            engagement ferme de sa part envers le Professionnel de Sant&eacute;.
            L&rsquo;Utilisateur s&rsquo;engage &agrave; assumer toutes les
            cons&eacute;quences d&rsquo;une non-pr&eacute;sentation &agrave; un
            ou plusieurs rendez-vous,
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; L&rsquo;Utilisateur s&rsquo;engage &agrave; pr&eacute;venir
            la Soci&eacute;t&eacute; ou le Professionnel de Sant&eacute; en cas
            d&rsquo;impossibilit&eacute; &agrave; se rendre &agrave; un
            rendez-vous pris via le Site. Il devra utiliser pour cela les
            fonctionnalit&eacute;s &laquo; Annuler un rendez-vous &raquo; ou
            &laquo; Modifier un rendez-vous &raquo; accessibles dans son Compte
            personnel,
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; L&rsquo;Utilisateur s&rsquo;engage &agrave; veiller
            &agrave; conserver son nom d&rsquo;utilisateur et son mot de passe
            confidentiel afin de prot&eacute;ger les donn&eacute;es personnelles
            qu&rsquo;il renseigne,
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; L&rsquo;Utilisateur s&rsquo;engage &agrave;
            n&rsquo;utiliser le Service qu&rsquo;&agrave; des fins personnelles
            : le Service ne peut en aucune fa&ccedil;on &ecirc;tre
            utilis&eacute; &agrave; des fins commerciales ou lucratives, et en
            particulier ne peut en aucun cas &ecirc;tre utilis&eacute; pour
            recueillir des informations commerciales,
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; L&rsquo;Utilisateur s&rsquo;engage &agrave; ne pas
            acc&eacute;der et utiliser le Site ou le Service fourni par la
            Soci&eacute;t&eacute; &agrave; des fins illicites ou dans le but de
            causer un pr&eacute;judice &agrave; la r&eacute;putation et
            l&#39;image de la Soci&eacute;t&eacute; ou plus
            g&eacute;n&eacute;ralement &agrave; porter atteinte aux droits,
            notamment de propri&eacute;t&eacute; intellectuelle, de la
            Soci&eacute;t&eacute; et/ou des Professionnels de Sant&eacute;,
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; L&rsquo;Utilisateur s&rsquo;engage &agrave; ne pas utiliser
            des dispositifs ou des logiciels autres que ceux fournis par la
            Soci&eacute;t&eacute; dans le but d&rsquo;affecter ou de tenter
            d&rsquo;affecter le bon fonctionnement du Site ou du Service ou
            encore d&rsquo;extraire ou de modifier tout ou partie du Site,
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; L&rsquo;Utilisateur s&rsquo;engage &agrave; ne pas copier
            tout ou partie du contenu pr&eacute;sent sur le Site sur quelque
            support que ce soit sans autorisation &eacute;crite de la part de la
            Soci&eacute;t&eacute;,
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; De fa&ccedil;on g&eacute;n&eacute;rale, l&rsquo;Utilisateur
            s&rsquo;engage &agrave; acc&eacute;der et &agrave; utiliser le Site
            et les Services en toute bonne foi, de mani&egrave;re raisonnable,
            non contraire aux termes de la pr&eacute;sente Charte et pour une
            utilisation strictement personnelle et &agrave; des fins non
            lucratives.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            En cas de manquement &agrave; l&rsquo;un ou l&rsquo;autre de ces
            engagements, et sans que cette liste ne soit limitative,
            l&#39;Utilisateur reconna&icirc;t et accepte que la
            Soci&eacute;t&eacute; aura la facult&eacute; de lui refuser,
            unilat&eacute;ralement et sans notification pr&eacute;alable,
            l&#39;acc&egrave;s &agrave; tout ou partie du Site Internet.
          </span>
        </p>
        <p class="c3">
          <span class="c4">5. Protection des donn&eacute;es personnelles</span>
        </p>
        <p class="c3">
          <span class="c4">
            5.1. Donn&eacute;es personnelles collect&eacute;es
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Dans le cadre de la mise en &oelig;uvre du Service,
            l&rsquo;Utilisateur sera amen&eacute; &agrave; renseigner des
            donn&eacute;es personnelles le concernant. Parmi les donn&eacute;es
            &agrave; caract&egrave;re personnel de l&rsquo;Utilisateur que la
            Soci&eacute;t&eacute; collecte aupr&egrave;s de lui peuvent figurer
            :
          </span>
        </p>
        <ul class="c7 lst-kix_uemhnfreao2g-0 start">
          <li class="c1">
            <span class="c0">Ses nom et pr&eacute;noms</span>
          </li>
          <li class="c1">
            <span class="c0">Son adresse email</span>
          </li>
          <li class="c1">
            <span class="c0">Son num&eacute;ro de t&eacute;l&eacute;phone</span>
          </li>
          <li class="c1">
            <span class="c0">Sa date de naissance</span>
          </li>
          <li class="c1">
            <span class="c0">Son genre</span>
          </li>
          <li class="c1">
            <span class="c0">Le motif de son rendez-vous</span>
          </li>
          <li class="c1">
            <span class="c0">
              Son adresse IP (adresse virtuelle de son ordinateur)
            </span>
          </li>
        </ul>
        <ul class="c7 lst-kix_fb51gelvlca3-0 start">
          <li class="c1">
            <span class="c0">Son mot de passe</span>
          </li>
        </ul>
        <p class="c3">
          <span class="c4">
            5.2. Finalit&eacute; des donn&eacute;es personnelles
            collect&eacute;es
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Les donn&eacute;es personnelles collect&eacute;es visent &agrave;
            permettre la mise en &oelig;uvre du Service de prise de rendez-vous
            en ligne. Ces donn&eacute;es sont n&eacute;cessaires au bon
            fonctionnement du Service et ne sont utilis&eacute;es qu&rsquo;aux
            fins suivantes :
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; Transmettre au Professionnel de Sant&eacute; les
            informations n&eacute;cessaire &agrave; l&rsquo;organisation et la
            pr&eacute;paration du rendez-vous de l&rsquo;Utilisateur
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; Permettre &agrave; l&rsquo;Utilisateur de g&eacute;rer
            &agrave; tout moment l&rsquo;ensemble de ses rendez-vous via un
            Compte personnel s&eacute;curis&eacute;
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Le responsable du traitement des donn&eacute;es &agrave;
            caract&egrave;re personnel de l&rsquo;Utilisateur est le
            Professionnel de Sant&eacute;. Chaque Professionnel de Sant&eacute;
            destinataire d&rsquo;informations personnelles dans le cadre du
            Service s&rsquo;engage en y adh&eacute;rant &agrave; garantir leur
            confidentialit&eacute; et ce dans le cadre de son exercice
            professionnel et dans le respect du secret professionnel.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            La Soci&eacute;t&eacute; s&rsquo;engage &agrave; ne transmettre ces
            donn&eacute;es &agrave; caract&egrave;re personnel &agrave; aucun
            tiers non autoris&eacute;.
          </span>
        </p>
        <p class="c3">
          <span class="c4">5.3. D&eacute;claration &agrave; la CNIL</span>
        </p>
        <p class="c3">
          <span class="c0">
            Conform&eacute;ment aux dispositions de la Loi n&deg;78-17 du 6
            janvier 1978 relative &agrave; l&#39;Informatique, aux fichiers et
            aux Libert&eacute;s, modifi&eacute;e par la Loi n&deg;2004-81 du 6
            ao&ucirc;t 2004, l&#39;Utilisateur est inform&eacute; que :
          </span>
        </p>
        <ul class="c7 lst-kix_fsz3ct13ih0y-0 start">
          <li class="c1">
            <span class="c0">
              le Site Internet a fait l&#39;objet d&#39;une d&eacute;claration
              CNIL n&deg;1672087
            </span>
          </li>
        </ul>
        <p class="c3">
          <span class="c0">
            &middot; les fichiers clients de la Soci&eacute;t&eacute; ont
            &eacute;galement fait l&#39;objet d&#39;une d&eacute;claration
            aupr&egrave;s de la Commission Nationale de l&#39;Informatique et
            des Libert&eacute;s (CNIL)
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            La Soci&eacute;t&eacute; s&#39;engage &agrave; prot&eacute;ger
            l&#39;ensemble des donn&eacute;es &agrave; caract&egrave;re
            personnel de l&#39;Utilisateur, lesquelles donn&eacute;es sont
            recueillies et trait&eacute;es par la Soci&eacute;t&eacute; avec la
            plus stricte confidentialit&eacute;, conform&eacute;ment aux
            dispositions de la Loi pr&eacute;cit&eacute;e.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            5.4. Droit d&rsquo;opposition d&rsquo;acc&egrave;s, de rectification
            et de suppression de l&rsquo;Utilisateur
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Conform&eacute;ment aux dispositions des articles 38, 39 et 40 de la
            Loi n&deg;78-17 relative &agrave; l&#39;informatique, aux fichiers
            et aux libert&eacute;s du 6 janvier 1978 modifi&eacute;e par la Loi
            n&deg;2004-81 du 6 ao&ucirc;t 2004, l&#39;Utilisateur a, &agrave;
            tout moment, la facult&eacute; de :
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; S&#39;opposer au traitement de ses donn&eacute;es &agrave;
            caract&egrave;re personnel dans le cadre des Services fournis par la
            Soci&eacute;t&eacute; (article 38 de Loi Informatique et
            Libert&eacute;s),
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; S&#39;opposer &agrave; la communication des donn&eacute;es
            &agrave; caract&egrave;re personnel le concernant &agrave; des tiers
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; Acc&eacute;der &agrave; l&#39;ensemble de ses
            donn&eacute;es &agrave; caract&egrave;re personnel trait&eacute;es
            dans le cadre du Service fourni par la Soci&eacute;t&eacute;
            (article 39 de Loi Informatique et Libert&eacute;s),
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            &middot; Rectifier, mettre &agrave; jour et supprimer ses
            donn&eacute;es &agrave; caract&egrave;re personnel trait&eacute;es
            dans le cadre des Services fournis par la Soci&eacute;t&eacute;
            (article 40 de Loi Informatique et Libert&eacute;s).
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Pour exercer ses droits, et conform&eacute;ment aux alin&eacute;as
            1er des articles 39 et 40 de la Loi n&deg;78-17 relative &agrave;
            l&#39;informatique, aux fichiers et aux libert&eacute;s du 6 janvier
            1978 modifi&eacute;e par la Loi n&deg;2004-81 du 6 ao&ucirc;t 2004,
            il suffit &agrave; l&rsquo;Utilisateur d&rsquo;adresser un courrier
            simple en mentionnant son identit&eacute; (nom, pr&eacute;nom,
            adresse email et copie de sa pi&egrave;ce d&rsquo;identit&eacute;)
            &agrave; la Soci&eacute;t&eacute; (dont les coordonn&eacute;es sont
            disponibles dans la section 1.1).
          </span>
        </p>
        <p class="c3">
          <span class="c4">5.5. Cookies</span>
        </p>
        <p class="c3">
          <span class="c0">
            Afin de faciliter l&rsquo;acc&egrave;s au Service, la
            Soci&eacute;t&eacute; utilise des cookies sur son Site. Les cookies
            sont des informations li&eacute;es &agrave; la navigation de
            l&rsquo;ordinateur de l&rsquo;Utilisateur du Site. Ces
            &eacute;l&eacute;ments permettent d&rsquo;enregistrer des
            informations sur les pages qu&rsquo;il a consult&eacute;es, leur
            date et heure de consultation.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            A aucun moment, ces cookies ne permettent &agrave; la
            Soci&eacute;t&eacute; d&#39;identifier personnellement
            l&#39;Utilisateur. La dur&eacute;e de conservation de ces cookies
            dans l&#39;ordinateur de l&#39;Utilisateur n&#39;exc&egrave;de pas
            un (1) mois. L&#39;Utilisateur est cependant inform&eacute;
            qu&#39;il a la facult&eacute; de s&#39;opposer &agrave;
            l&#39;enregistrement de ces cookies et ce notamment en configurant
            son navigateur Internet pour ce faire.
          </span>
        </p>
        <p class="c3">
          <span class="c4">6. Limitation de responsabilit&eacute;</span>
        </p>
        <p class="c3">
          <span class="c4">6.1. Disponibilit&eacute; du Service</span>
        </p>
        <p class="c3">
          <span class="c0">
            La Soci&eacute;t&eacute; fait ses meilleurs efforts afin de rendre
            son Service disponibles 24 heures sur 24 et 7 jours sur 7.
            Cependant, compte tenu des sp&eacute;cificit&eacute;s du
            r&eacute;seau Internet, la Soci&eacute;t&eacute; n&#39;offre aucune
            garantie de continuit&eacute; du service, n&#39;&eacute;tant tenue
            &agrave; cet &eacute;gard que d&#39;une obligation de moyens.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            La responsabilit&eacute; de la Soci&eacute;t&eacute; ne peut pas
            &ecirc;tre engag&eacute;e en cas de dommages li&eacute;s &agrave;
            l&#39;impossibilit&eacute; temporaire d&#39;acc&eacute;der &agrave;
            l&#39;un des services propos&eacute;s par le Site.
          </span>
        </p>
        <p class="c3">
          <span class="c4">6.2. Modification du Site</span>
        </p>
        <p class="c3">
          <span class="c0">
            La Soci&eacute;t&eacute; se r&eacute;serve la possibilit&eacute; de
            modifier, interrompre, &agrave; tout moment, temporairement ou de
            mani&egrave;re permanente tout ou partie du Service sans information
            pr&eacute;alable des Utilisateurs et sans droit &agrave;
            indemnit&eacute;s.
          </span>
        </p>
        <p class="c3">
          <span class="c4">
            6.3. Responsabilit&eacute; de l&rsquo;Utilisateur
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            L&#39;Utilisateur est seul responsable de l&#39;utilisation
            qu&#39;il fait du Site Internet et du Service auquel il
            acc&egrave;de depuis le Site Internet.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            La Soci&eacute;t&eacute; ne pourra en aucun cas &ecirc;tre tenue
            responsable dans le cadre d&#39;une proc&eacute;dure introduite
            &agrave; l&#39;encontre de l&#39;Utilisateur qui se serait rendu
            coupable d&#39;une utilisation non-conforme du Site Internet et/ou
            du Service qu&#39;il procure.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            L&#39;Utilisateur reconna&icirc;t et accepte &agrave; cet
            &eacute;gard qu&#39;il fera son affaire personnelle de toute
            r&eacute;clamation ou proc&eacute;dure form&eacute;e contre la
            Soci&eacute;t&eacute;, du fait de l&#39;utilisation non-conforme par
            lui du Service et/ou du Site Internet.
          </span>
        </p>
        <p class="c3">
          <span class="c4">6.4. Liens vers sites tiers</span>
        </p>
        <p class="c3">
          <span class="c0">
            Le Site Internet peut contenir des liens hypertextes renvoyant vers
            des sites Internet de tiers, notamment vers les sites personnels des
            Professionnels de Sant&eacute;.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            A cet &eacute;gard, compte tenu du caract&egrave;re
            &eacute;vanescent du contenu qui peut y &ecirc;tre diffus&eacute;,
            la responsabilit&eacute; de la Soci&eacute;t&eacute; ne saurait
            &ecirc;tre engag&eacute;e dans le cas o&ugrave; le contenu desdits
            sites Internet de tiers contreviendrait aux dispositions
            l&eacute;gales ou r&eacute;glementaires en vigueur.
          </span>
        </p>
        <p class="c3">
          <span class="c4">6.5. Force majeure</span>
        </p>
        <p class="c3">
          <span class="c0">
            La responsabilit&eacute; de la Soci&eacute;t&eacute; ne pourra pas
            &ecirc;tre recherch&eacute;e si l&#39;ex&eacute;cution de l&#39;une
            de ses obligations est emp&ecirc;ch&eacute;e ou retard&eacute;e en
            raison d&#39;un cas de force majeure tel que d&eacute;finie par la
            jurisprudence des Tribunaux fran&ccedil;ais, et notamment les
            catastrophes naturelles, incendies, dysfonctionnement ou
            interruption du r&eacute;seau de t&eacute;l&eacute;communications ou
            du r&eacute;seau &eacute;lectrique.
          </span>
        </p>
        <p class="c3">
          <span class="c4">7. R&eacute;clamation et r&eacute;siliation</span>
        </p>
        <p class="c3">
          <span class="c0">
            Pour toute information relative au fonctionnement du Service
            accessible via le Site, l&#39;Utilisateur est invit&eacute; &agrave;
            se reporter &agrave; la rubrique{" "}
          </span>
          <span class="c0 c10">Contactez-nous</span>
          <span class="c0">
            &nbsp;accessible sur le Site Internet ou &agrave; adresser sa
            r&eacute;clamation &agrave; notre Service Client aux
            coordonn&eacute;es mentionn&eacute;es dans la section 1.1.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Chaque Utilisateur peut &agrave; tout moment r&eacute;silier son
            Compte personnel en contactant la Soci&eacute;t&eacute; aux
            coordonn&eacute;es mentionn&eacute;es dans la section 1.1.
          </span>
        </p>
        <p class="c3">
          <span class="c4">8. Propri&eacute;t&eacute; intellectuelle</span>
        </p>
        <p class="c3">
          <span class="c4">8.1. Protection du Site Internet</span>
        </p>
        <p class="c3">
          <span class="c0">
            La Soci&eacute;t&eacute; est le titulaire ou le concessionnaire des
            droits de propri&eacute;t&eacute; intellectuelle tant de la
            structure g&eacute;n&eacute;rale du Site Internet que de son contenu
            (textes, slogans, graphiques, images, photos et autres contenus).
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            D&egrave;s lors, conform&eacute;ment aux dispositions du Livre 1er
            du Code de la propri&eacute;t&eacute; intellectuelle, toute
            repr&eacute;sentation, reproduction, modification,
            d&eacute;naturation et/ou exploitation totale ou partielle du Site,
            de son contenu ou du Service, par quelque proc&eacute;d&eacute; que
            ce soit et sur quelque support que ce soit, sans l&#39;autorisation
            expresse et pr&eacute;alable de la Soci&eacute;t&eacute;, est
            prohib&eacute;e et constitue des actes de contrefa&ccedil;on de
            droits d&#39;auteur.
          </span>
        </p>
        <p class="c3">
          <span class="c4">8.2. Protection des signes distinctifs</span>
        </p>
        <p class="c3">
          <span class="c0">
            Les marques, logos, d&eacute;nominations sociales, sigles, noms
            commerciaux, enseignes et nom de domaine de la Soci&eacute;t&eacute;
            permettant l&#39;acc&egrave;s au Service constituent des signes
            distinctifs insusceptibles d&#39;utilisation sans l&#39;autorisation
            expresse et pr&eacute;alable de leur titulaire.
          </span>
        </p>
        <p class="c3">
          <span class="c0">
            Toute repr&eacute;sentation, reproduction ou exploitation partielle
            ou totale de ces signes distinctifs est donc prohib&eacute;e et
            constitutif de contrefa&ccedil;on de marque, en application des
            dispositions du Livre 7 du Code de la propri&eacute;t&eacute;
            intellectuelle, d&#39;usurpation de d&eacute;nomination sociale, nom
            commercial et de nom de domaine engageant la responsabilit&eacute;
            civile d&eacute;lictuelle de son auteur.
          </span>
        </p>
        <p class="c3">
          <span class="c4">9. Loi applicable</span>
        </p>
        <p class="c3">
          <span class="c0">
            Les relations qui se nouent entre la Soci&eacute;t&eacute; etU,
            r&eacute;gies notamment par les pr&eacute;sentes conditions
            g&eacute;n&eacute;rales, sont soumises au droit fran&ccedil;ais,
            &agrave; l&#39;exclusion de toute autre l&eacute;gislation
            &eacute;tatique. En cas de r&eacute;daction des pr&eacute;sentes
            conditions g&eacute;n&eacute;rales en plusieurs langues ou de
            traduction, seule la version fran&ccedil;aise fera fois.
          </span>
        </p>
        <p class="c5">
          <span></span>
        </p>
      </div>
    </div>
  );
};

export default CGU;
