import React, { useEffect } from "react";
import Head from "next/head";
import DefaultLayout from "@/components/Layout/Default/defaultLayout";
import "../styles/globals.css";
import "react-calendar/dist/Calendar.css";
import { Provider } from "next-auth/client";
import * as ga from '../lib/ga'

import { AppWrapper } from "@/components/common/AppProvider";
import { useRouter } from "next/router";

export default function MyApp({ Component, pageProps }) {
  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url) => {
      ga.pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);
  return (
    <AppWrapper>
      <Provider session={pageProps.session}>
        <DefaultLayout>
          <Component {...pageProps} />
        </DefaultLayout>
      </Provider>
    </AppWrapper>
  );
}
