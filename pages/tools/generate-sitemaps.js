import { getCities, getSpecialites } from "@/db/common";
import { getAllDoctors } from "@/db/doctor";
import { string_to_slug } from "@/lib/slugify";
import globby from "globby";

const XmlBeautify = require("xml-beautify");
const { DOMParser } = require("xmldom");

const AdminPage = () => {
  return (
    <>
      <div className="container" style={{ margin: "auto" }}></div>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const fs = require("fs");
  const baseUrl = process.env.BASE_URL;
  const pages = await globby(["pages/*.js", "!pages/_*.js"]);
  const doctors = await getAllDoctors();
  const cities = await getCities();
  const specialites = await getSpecialites();
  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    ${pages
      .map((page) => {
        const path = page
          .replace("pages/", "/")
          .replace("public/", "/")
          .replace(".tsx", "")
          .replace("/index.xml", "");
        const route = path === "/index" ? "" : path;
        return `<url>
    <loc>${`${baseUrl}/${route}`}</loc>
</url>`;
      })
      .join("")}
      ${doctors
        .map((doctor) => {
          const path = `medecin/profile/${string_to_slug(
            ` Dr. ${doctor.name} ${doctor.lastname}`
          )}/${doctor.id}`;
          const route = path === "/index" ? "" : path;
          return `
                  <url>
                      <loc>${`${baseUrl}/${route}`}</loc>
                  </url>
              `;
        })
        .join("")}
        ${specialites
          .map((speciality) => {
            return cities.map((city) => {
              const route = `medecin/${speciality.slug}/${city.slug}`;
              return `
              <url>
                  <loc>${`${baseUrl}/${route}`}</loc>
              </url>
          `;
            });
          })
          .join("")}
</urlset>
    `;
  const beautifiedXmlText = new XmlBeautify({ parser: DOMParser }).beautify(
    sitemap
  );

  fs.writeFileSync("public/sitemap.xml", beautifiedXmlText);
  console.info("Success generation of sitemap 🎉");
  return {
    props: {},
  };
}
export default AdminPage;
