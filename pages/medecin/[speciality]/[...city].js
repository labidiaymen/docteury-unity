import { getDoctors } from "@/db/doctor";
import { useRouter } from "next/router";
import nc from "next-connect";
import { all } from "@/middlewares/index";
import dynamic from "next/dynamic";
import { Doctor } from "@/components/doctors/Doctor";
import { useState, useEffect } from "react";
import { SearchBar } from "@/components/Layout/Default/components/SearchBar";
import Link from "next/link";
import { getCity, getSpeciality } from "@/db/common";
import Head from "next/head";
import { capitalize } from "@/lib/utils";

const DoctorSpecialityCityPage = ({
  doctors,
  selected_city,
  selected_speciality,
}) => {
  const MapWithNoSSR = dynamic(() => import("@/components/Map"), {
    ssr: false,
  });
  const [mapCenter, setMapCenter] = useState(false);

  const router = useRouter();
  const {
    speciality,
    city: [city, page = 0],
  } = router.query;

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const activateLoading = () => setIsLoading(true);
    const disableLoading = () => setIsLoading(false);

    router.events.on("routeChangeStart", activateLoading);
    router.events.on("routeChangeComplete", disableLoading);

    return () => {
      router.events.off("routeChangeStart", activateLoading);
      router.events.off("routeChangeComplete", disableLoading);
    };
  }, [isLoading]);

  return (
    <>
      <Head>
        <title>
          {capitalize(selected_speciality.name)} à{" "}
          {capitalize(selected_city.name)} - Docteury.tn
        </title>
        <meta
          name="description"
          content={`${capitalize(selected_city.name)} à  ${capitalize(
            selected_speciality.name
          )}, Rendez-vous en ligne, Annuaire des médecins, Cardiologue, Généraliste, Dentiste, Dermatologue, Ophtamologue"`}
        />
      </Head>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1 className="city_search_title">
              Je recherche un medecin{" "}
              <span itemProp="name">{selected_speciality.name} </span>à{" "}
              {selected_city.name}
            </h1>
          </div>
        </div>
        <SearchBar city={city} speciality={speciality} />
        <div className="sub_containerr">
          {isLoading && (
            <div
              ng-show="false &amp;&amp; loading &amp;&amp; !loaded"
              className="bs-callout bs-callout-info col-sm-12 ng-hide"
              id="callout-xref-input-group"
              style={{ textAlign: "center", marginTop: "150px" }}
            >
              <h4>Chargement ...</h4>
            </div>
          )}
          {!isLoading && (
            <div>
              <div className=" row">
                <div className="col-md-8">
                  {doctors.length == 0 && (
                    <div className="alert alert-info search-alert" role="alert">
                      Nous n'avons pas encore de médecins pour cette spécialité
                      dans cette ville
                    </div>
                  )}
                  {doctors.length != 0 &&
                    doctors.map((doctor) => {
                      return (
                        <div
                          onMouseEnter={() =>
                            setMapCenter([
                              doctor.address.lat,
                              doctor.address.long,
                            ])
                          }
                        >
                          <Doctor key={doctor.id} doctor={doctor} />
                        </div>
                      );
                    })}
                </div>
                <div className="col-md-4 fixed-right-element">
                  <div id="mapid" className="search_map">
                    <MapWithNoSSR
                      center={mapCenter}
                      coordinates={doctors.map((doctor) => ({
                        markerName: ` Dr. ${doctor.name} ${doctor.lastname}`,
                        position: [doctor.address.lat, doctor.address.long],
                      }))}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}
          {!isLoading && (
            <div className="centerFlex">
              <ul className="pagination">
                {page >= 1 && (
                  <li>
                    <Link
                      href={
                        page != 1
                          ? `/medecin/${speciality}/${city}/${
                              parseInt(page) - 1
                            }`
                          : `/medecin/${speciality}/${city}`
                      }
                    >
                      <a className="active">‹</a>
                    </Link>
                  </li>
                )}
                <li>
                  <Link
                    href={`/medecin/${speciality}/${city}/${
                      parseInt(page) + 1
                    }`}
                  >
                    <a className="active">Suivant ›</a>
                  </Link>
                </li>
              </ul>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const handler = nc();
  handler.use(all);
  await handler.run(ctx.req, ctx.res);

  let {
    speciality,
    city: [city, page = 0],
  } = ctx.params;

  if (isNaN(parseInt(page))) {
    page = 0;
  }
  const doctors = await getDoctors({
    query: {
      city,
      speciality,
      page,
    },
  });

  const selected_city = await getCity(city);
  const selected_speciality = await getSpeciality(speciality);

  return {
    props: {
      doctors,
      page,
      speciality,
      city,
      selected_city,
      selected_speciality,
    },
  };
}

export default DoctorSpecialityCityPage;
