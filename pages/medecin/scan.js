import { getDoctors, syncDoctors, updateDoctor } from "@/db/doctor";
import { useRouter } from "next/router";
import nc from "next-connect";
import { all } from "@/middlewares/index";
import dynamic from "next/dynamic";
import { Doctor } from "@/components/doctors/Doctor";
import { useContext } from "react";
import { AppContext } from "@/components/common/AppProvider";
import { getCities, getSpecialites } from "@/db/common";
import { scanMed } from "pages/api/scrabber";
var slugify = require("slugify");

const DoctorSpecialityCityPage = ({}) => {
  return (
    <>
      <h1>Done</h1>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const handler = nc();
  handler.use(all);
  await handler.run(ctx.req, ctx.res);

  const doctorToFetch = await syncDoctors();

  for (const doctor of doctorToFetch) {
    const { name, lastname, id } = doctor;
    const fullname = `${name.toUpperCase()} ${lastname.toUpperCase()}`;
    try {
      const { spec, city } = await scanMed(
        "https://www.med.tn/medecin/recherche/" + fullname
      );
      const slug = slugify(spec).toLowerCase();
      if (city && slug) {
        const result = await updateDoctor({
          id,
          slug,
          city,
        });
        if (result.id) {
          console.log(`${fullname} has been updated with ${slug} , ${city}`);
        } else {
          console.log(`Error with doctor id ${id}`);
        }
        await new Promise((resolve) => setTimeout(resolve, 3000));
      } else {
        console.log("Error !!", fullname, slug, city, "-------");
      }
    } catch (e) {
      console.log(
        "🚀 ~ file: [city].js ~ line 119 ~ getServerSideProps ~ e",
        e
      );

      const result = await updateDoctor({
        id,
      });
    }
  }
  return {
    props: {},
  };
}

export default DoctorSpecialityCityPage;
