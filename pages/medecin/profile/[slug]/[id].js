import { getDoctor } from "@/db/doctor";
import nc from "next-connect";
import { all } from "@/middlewares/index";
import dynamic from "next/dynamic";
import Image from "next/image";
import Head from "next/head";
import { capitalize } from "@/lib/utils";
import { useState } from "react";
import { SearchBar } from "@/components/Layout/Default/components/SearchBar";

const MedecinProfilePage = ({ medecin }) => {
  const [displayPhones, setDisplayPhones] = useState(false);
  const MapWithNoSSR = dynamic(() => import("@/components/Map"), {
    ssr: false,
  });
  const DOCTOR_FULL_NAME = ` Dr. ${capitalize(medecin.name)} ${capitalize(
    medecin.lastname
  )}`;
  const {
    address: { cabin_phone },
  } = medecin;

  return (
    <>
      <Head>
        <title>{DOCTOR_FULL_NAME} - Docteury.tn</title>
        <meta
          name="description"
          content={`Prendre un rendez vous en ligne avec ${DOCTOR_FULL_NAME}, trouver son adresse et son numéro de téléphone - ${medecin.specialities[0].name} à ${medecin.City.name}`}
        />
      </Head>
      <div className="container">
        <div className="row margin-top-60">
          <div className="col-md-8">
            <div className="bloc-profil-doctor">
              <div className="row">
                <div className="col-md-2 col-xs-3 doctor-bloc-picture">
                  <span className="big-img-rofile">
                    <Image
                      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQcAAAEPCAYAAAE4/zFcAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAQhVJREFUeNpiZKAASO+41ACk6qHcxqceeg3kmMNIoqX/SVEPdBQj1RxBquWkOoaRlpajgUCgYzaQ5AgqOwBvqDBS4oCornAU/rKylWQ5hJEcB3guKGcQfPUAq9wvdm6GNfnzSHIIE5oD3hPjE1wOAAG2n18py6LEhAJ6FLT2TELhV5fkER01sNBgIScholuMLgdzCDFZH+QQJoZBAJhIDQUtHT2qO4KRVEfcF3tJlDrFV+KkhQQpgBjD9V+LkR4d1Aaf/jPS3hH4QoOUaIA7ApgeHAZF7iAHCNwTxRB79IeZPEcAC4sD5GgU5MF0vxzLX4b3X/7RNiSeuOuC8WFDcYZNTQkY8pdzylHUEQtYCCnQ5uVg2GmlihkS67czXA70xBBjOPwcxdEgMOfhG4aGG8/JDwlsDpj26AuYluHnRsG4QIq8CFHR4UhOuvhR0UiVlhYTlEFS4syS4wHTfzW04WJfF6wB01dsJbHqsRTiJi86Vpsq4ZQLCEnDKfZ6y3qG/1+/EDQL1p5gwtcAxeV6RTExhouHNoAtPVnaxvDyPxOYvWHNLLAc18cPDB+bq8nv/CDXqPiy2f1Xr+C+B1mO7EBcQGbnZcJtTChYgLVmtAsAY1o0+5mwKEgEUh/Qxe/ko+YEeVFRct0gSEq/M+E/EtCz9f+PDu69fAkWB9EwjA+QnDuAIbKAEQhIquLxpAd8ZhEsMaGaG/MSQrHKL104Aafed2/fgagDpHqGIMAWRQ9evSI66ActAAggsoMJ2iIDjdKA6APANORIc0cALQUVEuupPUpDlCOAlt8HUgoUhLYg0EEfBsVIDb6QwTVIIgCk3lM7AeJyCLYKzABInSd1pIbcURqyR2rQxyhIHTLCO1xEqQPIdQgLpYkQ10gNEbluP6xsYaGGxWQCB5qOT5DQKb4ADA1DknpgBdzEjcwtFyQ6dxuQ3A2c8JWbKHWR7wVJihcmWg0fk1AnGVB9uIiMQZLzg2cIkVqhQc5QEUUjNb9fvEXlP3tD35AAjcawSgijiLFKiZDtCKJLzAYNSfg4A/fai1jVgEZwkJv9yN0+ihyBqz8KGxrCGKnBohefY8DjE/haPHMN5bGPT8jzgi1EHqURmr0ILPcPS2uf0PgV3jThLsaHIfb8wwcGaXbMocL/IpC+6cPXr8lOmB+IHiL69YthfnY+VjnjdedJshxlkATIIamwd3K0QhkigtEaGxeC6VdeduT1O9CmnAnGJWysYjfPOwbXL0JgNmj0BuQAsW2HGH5fvsDAqmuAM6cgp0W8zTtiRmpq6nsZWhqLCfbM8TmCiZgmOb5RmsqyTEQCI7LzTcxIDVaT0EdqYICbm4uk0Rts5jMRoxAU11+D9enX+UEfi4Cxz1y4wmBioAOX+/TtG8PbL19oP1LDCAUgdnJeDYocHxcX1gFVfGZQa5TmPvJozNPnuAfLhtxIDUAAMdLaAuiIzn4KjflAaqk+oAFBhUEdUoEjuVN4VA0IoMdBnr4/WJI3KUN1VAmIge430iJQSBlIJXowbSgGCCM9UwBo5VPIxCSi1W9P6GR4L6ZAlwChy4Cy+pltDMb7FpKt/5WcFsOeiHpqOAU8OkZSQAxEKnB282RwckMsQUAeDD3hlcVwT8eeZqkD25g2RueHEoA+/Fzb3MnAwclJkhmkrOMjNzBYaBkIMJCSmcegqKxCtn5S1hKSUvPhm6aneiCAeyEUBAKNW73/sY0HUL1toMXyh2Gr0FuqmPX82VOGVfzqRA+qk5pNYAtB99MixK/9YaGaWZJS0jQJBJQUQY+WIrGLWLFmLTKHyUnp1NFtcqH0Ex9Z+tb84KSH8wQYqdRNJgrwMf5nuCj6imj1kR8EGU78YqNLRNF1ugm0apnYZA6atKFXINC1jACBn7//M3z/9Z/hveJruGfRAYu4EAMjMxN4jbHeIyGsS3+HdIoATUR9+/mf4VAvorkNmiVDx6BAAAHQOmfQclLQ7FqRCs0LSwaq1W/9OjIModK4R9NgUwD8MxcwXE5PYGATFmRQb6zAUPdkySqG9yfOMvBU1DGwSkPEipTFwBhrdvvzl0Fr7zVKnV8ICwiQD8haOUTsgvDTH38xmPKzMTCJCTNIS4oxMH77yvCivJ7h/fefDH/+QXKmECc7AzcbCwM3PzfDV3NL4gpgFmaiZhgJNKgmwKa7PtAyED58/QoOCNCsJQh8mwbpkrMDPSHBywWfweRig8QLbPoM3qr88IGq7kEDhihlBCXjfYTAe2BAOAmzM0x7+Bkuhu5ZGIAFEggsfvqVIVaaGzzhScMRrAsYhSUpgYFv/Ts2oMHNyvC+qRJFDFtg/OdCmLs2v5ChXIm0hhi+5fX4uuJEz/pRYiFoNlCQm5th09rZ4Kkp5Ompnyk5GAEDUwNbAc9++wZ4chc0sUtolpnYCCK4LZTY3iiZ+RFee4CL6pJmhvsPHqPIIy//B/czoBOJyAEAmu3GBwgUmoWgwpHgCBVaYOBch4orIGbMX8HAy8PNEB3qSzAgYLEfEeYLxlg7XGJiJAc2roDAl9qZCCSfD1DNRBXboJn7Yj5NhjQmOZL2WlAzEHAFAKEsz0KkQYLQFAKa1zDApU5KAujwiZBBrubKPJyeQ08VNKwRiC78WUg0GDEUjmXKe/uqWeCUAFrNQPFAjKAgXQKA4r4G0qIDDBd7huFftABL8mlZlTjVcLCy4jUDukUFGQRSshCC4k4X0N4PjEgAmBoSn70gnPR/ffrK8OrVW4xagoONjaiyQUhYaAIjKtjAMBQAMCcJAPH7mraJ4L1AoYkF/4kEDgyjgH4AIAB71xobRRWFz+5Oax+L3XZR1FBTEAPBGKmJ+AJM7C8lBiWgCWjYAjFRDKk/jBFSBU2oaBOkEIhmTU2IUQyBIIq/5CHGWg2hP4CIim1Co9a6bam726XtLs6Z2Rlmd2d27r17791u4EuahZ3Hnfnm3DPn3j3fuZ5iNIqCPvVjDVgkAyr6MJ5S/w7zSv6YUkS4vXJJ5wvsosEpT4TgqT/m+kdSiBAlC+MdLwglotipRDwI8ZQyATz9iKdUuoFo6/AykHBmqpJQiJV6SrgruKEZtf/ciSgxEgx8ki4NwYcI3iRghh1m2tkBE8YwcUw2GR6ZJJDomq3gmDzmWNWQyFmSltcTQQLrMQ5wLb3gzUMCDogCxSKBNxlulp3PIrgkjzx0dE/B51j24SvCyVBE+4XZZ08S7YdJqK9t2mImo/Ze/B3Ce/XqANWXB3ldjlalxM5fKA5dQgqWLlsOjyy2bw5zMzFL953W13k3e8juJaGI6hJ2yM61JrESQdHncLY0KjsFuVNEw4UUwDDSj2dcOg8D9fN5XVLAzVmG4DpBdmjgLYZvYAFHa7C1Cq9I33Aq+C+X88y6626q/EwKqwgxD8NpMNOX5HYuTFYVgM4MIlRmdsB1DsMiWkQ1UFtXV/A5ei/+JtJpPm2OPkXONRSSlW+FyLzsKVM9nASi87KxGlSLyAZ4aC0k6DU0HyHcUS6OsFcm2hnzS3WWQtGf9DEfK1K+JJ0IVquQZQ0o85RGBItVyLIGFY9JfWvQOD3ahRJKpmsYuDoxSbQf7WINJUUEJiR6yhRIXnYuT5lKjGOlNUim5D4gRWZjIzH97nw11ZAciUIqnsjY7vH5QJmhTxyNxvV9JWm6TipQJPgCfu3PcYyiEsCydgUjTkih+/k7bobGm8ph25e7iI/BNTU65tfCkpppUF8pNrzG5DW0iA94jj5RY3W+KXM2aXB0FBp+7YWO97dpa3Xcu3t73nMMd5/WCjU+rv47/OcIdDXNzdiOa3jgWh5cnaXKxqtc4oT0yifZJCCiiUxfgCUxx4fsf03Ebf37vjD/X1OWG39gnU+jPV7WovAggAZ1y1fA0MEDcOHNdx33mTZT1zmiIM4NXUt0a2FV+HF5fbKoeK6+sM5U9VVanjYq/ozvlV1hk4iVt5QJuxYjgLVaBApTAiAZwaqKvNsPD8Th5QfERpiGa2Aqc1oonKSOWkB1Z8O1i0zQTf7SKg+5dA1Reu2xt9uZj6VRHqbRY0cEld7BSatNislF5OtzJCYmRHWLxhwi1C+fEdkd5ux8C/4ZvFZ458r6DcTk/DUsPptR2qALyye/+NIbefexkoMyyPvC20U6Sef6vyL04YZZb9wQ0j67f+pxdZqxWFz7tC47hALYSPOzUi2ij2cDKFJDxR5qtVC/1fbeHjjy9bc5+42v0jMAsfusXtMCO9pbzW3VoRXaZ3Lgb+rC2yTWYBtZqjvN4v2Dj0ZGQA9TkAw0+48798ODCxfA1nn3gO+Xc7BvJAWfpzXjH+1tg9tunW5WmMaffw0ZNAcibMW8UrXhlyIRmEzqsQFqwlEbbsW65ufgqaVNGd+h4s9688rsOVC323lJ4pU//wFdQzEqa3Aba3CPNg0StBtsqDet48im9ZC8f2HO/uWKfnluovjMoMrvSASTNjxftOkUwf0XjUHL5jbnLlJuP1K0I0GzSoYfkB+urc47pmB6fToxiKxno7WtAxY9uRqOn+rWZNF2IljDT3CJQMfGqB6S23SD6zAcycj2F7hIas6M0jfHzBL7lf19miyah0bckYj4GFQSZt2RhAVelhPhLJQdpn+nrzdy+1efaTUk3HDsxA/MRERyteHMJFBFltYT2pkfPv3AmR+1UNp7JQHfH/3UnYjjXY7b6oPBvMcOERBBEyBShdhuJz4QbjdJcYK1HMLZcxe0t4dtn/X5CrGIHtoomXqska8BkqeUXQ4BgyqRo0qhg650jYZG1n5rhVNZFUZsZa0fwTx5q7aHoyePiMU0gn4/68Mp3jCcpYqHv6Iip+xSxlupqkpo+1KwduNm23WNs4H7hPcfyljn2G2t45Jc0kW91gVON/HoE6tMsoajUY2AZCpFcv8hKHWoN7HFsAQSi0mjE27gBoqC/wVg72pjo6ii6N3ttlt2S9ulLeWjhCJWYikNJRACSbXUgBCjQkAk4QcUUARNlWIkJEQhYBTFQIGgNnxU/kgKYgkxCkRogqJSE4gWA6KwDYVIS6GUftEW6tzZme10d2Z23syb2ZlpT7KhbKc7O/PO3Hfffe+d47DbBXH7TlC/Ru94UsW8SiNtrB0ghDGNjss5zL6zQvH+/AFCkDU+7tKca5MH0I8jNLVSy/2SENwexGToP5gRDRk20xJCL/Mjq0YQnGXsd4Tg7H4KBtpfFk1Gr+YylBAWSQTNiotqStGmJIRFFbzMjDEMOfyWIoTZPIJtih20NhLoRgir2QVnXK0GX30tpN+4FHwPHXfxVZc1GTrdXitcBtV6By2lW9PqnKJG0XNfb6L2eTSNeCmDiia0gwIZTJcjyIlk0gRGEvTxNhl8WgpfDg1EMF0FcU75OqYL8Bt+Xuxajry931SsULuW3mGXqEBRAFU1kBQmyzuIq6BOQiJs7M9kQHE4OSwoXWa2W3OGVKmYRBHclHMMKKKrVD9WDtk5uTD/1cVE6qEorGmF7oOkC4mKMnq0osOkKVNZaVmfbwi18587WwXfHTva5z2KQuaGJ5wuK5NBDPMXLYZJk6cadj4UbX6aiS7bPuwd8aFadWtSmhlvzz2mPWUrnU67kGH4iJGsPrKRZAg+dkzEEap5JzQ3mPlWXecqyWSEsFpkeKtkXVTPjxLvFsJ1IkIMTEipA5+Q6qDXTR1SbewUOdASa74XxLdT0xCm121lBB4/5nt9mdRkSVKIRYilViBERkxAsW7FqmJTfS8cySASndYIsqEyz06rdhW83itasJimY/73KlvLQKxsssyy0O2idQhuP8MZsBAwLM9yPzTd90I5WqO1eTUiuCJLVz8RvbHyfrLhWsBKYDEyICYqqkNYAXjzUVUbzSWiif1tHvZ7GGFroFMusT1ICG4VtKWBJhvRjBabWwZb/Ra+I4wQBXaoA0QrVFs1KiiqQ1gdRjeOnciAAwvbEYJvJCO6DzuRge8pnGa3bdTSfWhxOeqHZEA8a8sIwQOdnnBoShPo+GJTMtgzhwjFyYdu6PqvUbHHkRy6bt0BZ3ycre8XLpC5aNeL4x16Yp8ICD6i19Pj1nZweuIhJtHLPA7yeQZ/PB4XOywFYkekwk8dscHPTfY6wWEzDR7dXU2jTQZsNH863VlR7DJaOnqgq7uHJQSewyYosmWX0c1Ja7tiHLo8wRNqL0N+qhtiXQ7WaQ6JYQfcnJ1bHlV7HS1YMToVXhnp66M63dTaCveY17RfAhHh3OVj8PB4JcBuerurrpV+CTtmz4W4LB/cGvUIZlXXQ95gL/zwzNg+xzUzrNxb2wh7/XfYn62UQyDQR8VU5Wts6H15oyGDwGqsnXPpeNAd6C48y1ZC5+lTrL0ZIvvTjRBDsMxeiNqyg9D8xyVweL2QXDiTfW9EfGBYe7ahJex4VAVH4x0p8x30lENvOROhKphDmCGPQKskLb5RiOv1ASH+nLOBG12TPzyQT8ybI/t33qxem6ZH7R3QUXdL8lg0D+SBxMNolJ+WEBYh1CD7x7+iFk34fRvCZfhU3RuVRoET07N0Pw82ItrStHV2w9328PUTrVevyf59gjsWkpnhJnqDCZ1FTzcGPmuCh06iwhs9Hr55D9bU1EW1y2BtJUKXU+kJdJ3Ugwy80ePq0X1nH7ExPe+/C544dVLnPalD+7jmISpvB0yhpiTRrU1gboQwkBTBtQOho4x5Rn0D3n2TFniXL54QoY2EjUlihheKtm17JMmH52pua9OFFAbAL9zNFWrHVgkS5lS0uwra6OiMbAErZgqoiAx7vop4TGNLiy55lQG5Qx8ZRKfIAbpTE0cPtIHDTTY5dsf0eXrD8gUZI1UxoB1Wj0c62R3s0q+UozXJJukqJAkhzDj1QoaOHudj79xgPY5OlpRIHkNCio7i90Tff746MKL5Ji81+B5anlsIRWIbf+XoPQYsiJzxTwV/fn31euKGVkIcdAwbtHUDSzy+FoEItX6nAZ3clFGorFzsF3IuhX49SKGXXXSf7P9IGftvfX2jJCmwK5ByR0R0zXpB9P1DFcdh5+7AvUSj0VA8pmxzo9VVWoIMkqp1EbsG2gpzvzKjC9pdBj6ZYuF6LucQi0CrXDHXN942V0l0EH4eTzohPKuXgKMt3DvUt3UnxE5Qb7SWceJPWrcqolIdiYIMFeqrccTVAvRWRVtRIfDJHpqWIkkKIRnQfVdoEVZSvByWLAg3j0NDavRi5oEmtEJH3pQDFRCTPiyahFCkN0WUPNIghdGEqG1oCIZx7Pv5cE+Wl4yDLZvWhr2PntRObjq1pWwXtFUeliQEiUsxbUKQDBKIRxNa7QyMIgTaS6PXdiRs+OAz1kQUcSrhLvGwFL25hQg1KY/LzYPkj0u15RE1dVBxU1WvTayor0WnssfsEYKf7JIDnxcUzpgOxW8uJfr89KQk8Ljdul/HifpmWH6hlvTP0LmHeDWcS0sYYkiBd1CxngRJoWXhsjVw5Z9woZPN64vhpTmFihvs9v37io4tLJhOfA+MIIOKuo0mKwVaWtcoeB4xjcYhp5JhFHrKswlh9kS4PbN3egXH/Qh0F1dirK0kSvARQmzUIAcfrovwqqskoh30kBQyJTwleQSNgiKVuisykvsysvMg03xkN1BIBvY884uCIweShpNCzaW/VV+zWjKwIxIVttiRiECruuyi/MV8XMQQFTkdn6hsUmtyXg78fqEmGBHaMzJhUJ0/+PvvK8qIGo6f5wgnxBVV14lu1VpwlxIh9JhicIEOEBAD84tgpobLypRgX+kW9t/z1RfgtbWbWDKsKloEbzAvNYiPixOdDVUbIdISEzXdH40RQlcvLl1XXWOJlGOxqgtI8PSuf1RLBsTw5GTZCIF1BhJyRQlFXNeg62y0y4gr4WbV2BJOT0+PYke+BxRnD7GAJDXPsGjhi5rJpSOaHA6HYatlDN+XwVzcDAcH4Fb6KgmtXxw4pOm8WFUU4rfzvUN04Qyp7NMTo33zsNL8wdELQ6VxorpRJ4QcojNw2VmBCdfPNRIiFB99ElgShwUppRiVkqLn7SgXkCBqGwRNuzOR6Vo2Alci5+sSWJDCwpRaYJeBcxuRZi3FkOjxQEpCAs1LrMIHwmz33TJbVXPzX8bO+9txT44pqNiv3g+WJxfi55OHJFc5YfdAMSJgDoXR0PQbqy2/d5mJJEiU7RBBgRenwPmCllSk6ezuhjiXpjwbc6I1Vmh42xKCIMJkjs0cVXD04K5M5r/os5jJvSI1sJ951XIhvgoGMIABDKDf4n8B2LsW6CiqM/zPZjdZdvMGNoZHTSApFpCHFamWR4JPQq1QLVrtqYly8IgohPqi2hJ81EetBEK1hyMSPKVVsYeoFatWiOipIJ6CGDhalIQKKAkkIe/3dv7Z2c3sZh/zuDNzZ3a+c+ZkMzs7j3u/+7/u/9+JG5XBL66G21TwzbMUxFAVCHy7G9oDB9V8AbtFBvU6HWtTbwURU/EKPQZMgao0E0kYE3Q+ehYrKbgVLH8sNTI5GAN2PnoDO1Qe+SQkRwlfH2uRQQUSUPducZFASVFoBInBUE4ANPIMvyq/ADGLYiwyDCVBMUhI0jUgopbNWWQA5XUdBkQ1S4pCiwzmVgdSUY5LNsU1GUgXCZsAomor1YJNRyLssIgwBLv5TPT4kAx8nKDW6veYwABWuWnJEJpmbyEmmrVYw0tzMlgvi1eEXC2CVowGJMCw8QGrPxVD9YAVozIRUCVssfqRGBRVZ+tGBgPPJcStHcGoRARRpf0W5EONIl1GBSKErdi2QD8hmHglQtY3R8DzvyPs38NB+0+PnQT135vI/p0Yd4Rg4oUIGfV1MGfHM+A+1yDpdz1JbthbdCecyJ9hekJouvyPHvjRzudgXM0HRM7V5MmBt4ufMi0hGAJEEF2GryV+uGsrTPh0pyrnRjXyr5vWmI4QjEIiYCLqOtoa5eanb9TkOq/fsRHa00aaxu1Uso4kdZHFxO52uGH9bZpec2/RMjg2eS5NzSA7i0rJFHbcE8Fvk6BxShGK+WQhbchA46STHkTwY37lA7Q1x25NyMAbjNR5DPFMRlID1ibxAtNo9BxIuY5K1RRt4OeHVJMMB+KVCLnj88EZ45XNGNSiDAv5PFNRsEtgGZXZy2qoCOz4y6+az/7NC/v9M78vg6bGoau6jTm6n8YmahLrNdpFEiFWCbuhgSN+wXU/g4suninq+Ht/UwZ/f2Ub/Gf/PkM8HxYni0nFt0tgl6mAox8JkD1qtKzfX3/jLfDtyRPw7amTQ9QWZXEHBAYHY5JBzAvQioHSbCVseLFqIiMzE+axol/s6BeLh+4NXjiM1lA1+F6nnKtUMlCbthZNR+PIv2z2XJg4eYqq97B81QOw8dnBySucFqcUOajuw700VRQZ+NR2ahHqzuHoR32uJeSqGRqNyViuZTHNTyYchdgpWhPBjyV33mMYNvBFTNLIQLtUCCeu9TNG84zUVLVyJEMxWJDknhpIOqSLJgPvQdCvAD05AWNRb/x4doHhpYPNaB6EEL1Ol8/FpEBMT58x00hkECcZpMSy9QZmMtMiGTIyMoOklQFUxRYxksEQdZET7X2wfko2dQbcggk5sNLdboQmLBZDBkNQ+63Ms1Ra8RjkWuFuM6SbaQv50qqNjC83c0c0yWBVTMcXpkUjg+FqJDEE3dTUaHUrAVVhE+w0VNX03l7fS0lxJrK5kQ4yqD0pphLWhZMM64z0BFs6fDEGnJKu/fqo7veDeQ1ITMRrXcOM1JQBO5ERSAbDrblU6zlNpxFZn2WodvSX5dnAwDjRnwAWiNgNCwNkkFuBozcWNA6n7p5+0ZxhxKZcIZQMtxrxCVq8DFX6+UifHfb2JBqxKQsCNoMZ1mjU234wmp0Qzm6wgUmgp4RACWUGmIYM97Wk6nbtO84Zfz0ztBtNQwY9YVA7YYjdYDOqJ0HLCDVYgCkarrOBicrm3u1Oiiv1RBjTkAxzwUTQ0s83kVQIGJCmWtZXS/1tIqkQIIPplvadfXaERYR4di2FwDkLtX1/s6kI05IBMbXBo9q5aZwTscigg3eBEgfnICwyWHEHVSWORQaV0fvdWWLn6mtoNnVbmZ4M9uFpRAjR39wGTILNIoNR0dblBcZh9xHi1BmAAXkz9RyZBgYgITMVevvM+0ZGu1kfrLvXG+g4JITjvOG+TrUxkJCeAjZn9OCUt6cX+pvawNvfz5HAfzwSjP0WUEikumwWGYyAju6QEcySwDFqBNfJfY0t0C9CSiSkuMCW4gra57AzHMn6B1jPomPAVIRAMlSDydZ4xI7iJEKYuBOT6OCkhFwkO/GkDDS1DXDX8XrDX8eoNoPpTOT2Lh8b3Enq9ZLb6Tt3a+eAqQzIz8xGBr9kQJFOGhce/wKmdjRBIn/ufvNwgSNDtVke5sPpWdzmxxuPFBO/xuNbn4SKaWODrrN5+vlwtcfwE1fV9pPXTKke/c9DhrrrSzPdsHhUBvx8dHDuQm19/ZBjj296Cc5f+iui12fc7qD/kQihZPi4sR2e/fo099cg+MAQ3gQ29Kq8LJiU4ox57BftvdzfGWmJYJ88BVoOHYKuE6fAOWaU4vv4fPkD4Jh5qWjCbs8cF0SOsi9OweHWLlqbuZJaMpRdkA1Lzpeel9DKxxYuSHZAyqNPQdOi+XD0yfUwonAWZF9/rTwbpLMTjtxX5vMmHvydbGn2zmW+tadOdPbA7QeOU0UMVkPU+clQBxQs38ONphnjFJ1j/7ke7m+KPdj/P7P7I27LKroSPEVXiDoXSpRj6zdxZCCJMcMSA8TYfrIJSmtOUBNnQGxltzVGJkFPX59PTbQNqgnOBbx7FbRXPBs47vTO97gt4CLmB1+3/eixiNdInHcl8WdHuwe3F46fYdXIt7p7EygiyvS6gb1zJigmAmLA6+XVhM/Xu8BtD3SgI8oEE3a+cIvYUAzDEUstoEo8cfWFenVDeYAMeuHI5RM5kUkCXT09EdXEyIk/gDFp7qikiEaC7BQXjEp1adImOhFira5kQNWQaie3vkJnb29kojzIPStkJQ/jSIEdm+pMhKQw18d9+J3w2AQbA71XLYh4/o7ubuLSUmPjsVloM/jZoZndQEI1CNHX3x/xO6/LPWS0pyY52J53iLdJbo78RuFzrIHpSiKXYkdKWsqyGfS2G9QmA6J7yXLVru1XUaRjKxqhdAgZtFYRmpNlVoF8TyWKVBjtVGcpIQyyaaQiyiORoVKLG1g1Xp+FLbwj5CWzhrMX/IbqqCR1yCAm2qqamuBZUmJWycAZmY/8gdi5/GSYkZ4UFOcwGMojksHsCDUkxSCSrbG/2edB+INbpD0KBElvK4KKKI1FhrVmJkQ0/S/F1ghIBp4MrV3k5xkWj9Z25ThbGLao6lWooSJieRKx9D8JSSLlHijxKApjkoFHnXpkSCZPhoHBdKO89Wu4bcOfKhUbkt1L7tJViqlpW2Eei1gyTFftATPIP6DQz/d4fMmuu3b/O7Ih+aA4Tdh/0SVh9z/3jz3c34VZhq3EXitKTfCsaTYS24Wh6NuLbxThYo5UpCLerfwLJ33aN1UE7W/r6jIEEyKZAtG8iVyj0HxAoCZmXjK4EM3Da/4Y2XZYuDi6tIkgPXZVD0qcx9b+Oui7VmOQoVK0ASlgT51RyBDq40+e5JvoqTn8ZeTfxCDDwPdywu7fsHGwLd1uV0R1RbFUKJFMBqNJByGEI3bhDUslG5KR9pfe+2jg87at5WGPaVx+G9QXzeG29m3K3/JEWK1WSnItw0gHQxbZrL5/WeDzLbeulGRIhtuP3klt3TcByRMqFZiOdnAX3wB9x74K7EMyIClo8b5iRZhtIk5A/Zr5uR4PZGdkwPDkZEh1ucCZmMjZDrk5Y32d0t4RVkJEMiRD96NE8HsnSIJQW8GekACuZYML8ycvvRucV8wPIoVcpNqJBYlLYx0g9kpVtPvNToeDIwISIjs9nSNI1UsVMCEvN0hlbN7ySrAhGRKEEtoKNYf/y/3GLxEQH+3cxp1buI0dHly76RifDwlZ5xEhw6SUYaSkQnmsY0TXn5F4DQHm+WEKvJZAd6/ihW3w8qtvBu3HEX7T4mthXsGl4LlrsMjmk5KV8Ldd+4YYnyhl1j3zW+7zyNRUSHYGzyr61YFn5x5R+6VgzDufK22GDDHhAilkQJ9N0auRcY5+1Xjt1kRCL+Ok4M12S5ethvp66au4bHr+CfCMDB79qIpQAoV2OtfyT22AnkMHAhLBPi4PMje+qBcZqlkiFIo5UFJlKksIJIPsFWUx1U2r6WvMlj7e0BD2O1QVb771ftTfo5G4+v47hxiKkQjRtqkCOqq2hz1OiVRQSgb/y8iIk0GputCSDOHqLsPBb1i+l9wIHc9tlTQ5hUYr2ioBlRSGECO37wTGnawXGTKkRJPllNehRVZLmwEZCkxQlZJj0DerUHK+g5AIfi8CN0pQLnVawSZD7GDsoRwoR5ordp0Degt+6D1DSRjNoYkrqpCBJ0SpFsGo3R/ug5UPPQF/3vIynPquXtGoDYf6hjOy7w1jC7RCbmzIruSCar31rrWtHWYV3RJEiudZQqQkuzk/nxTkeBYByTNMnv/f29sHDoeqxe+yg4Q2hQxUZdEkIRGikUTp6BWqCalIdckrt2ttaVGTCIVK0g/shJjYROppnq7YHPj81YrBOYKkhu9g7F+f5wghZfSebWuL7HEIIosmQGm47CXNJAMvHZCJxDKjtm33RQpbJwaHM7pHDoZ333h7F5HRi3MW/piCVjh7tlENL6xSTLhZdTLwhDgIKqbKhQJtB5LIzRmjiYpQCVWk6l2ITYmRIsRP58/zdfiRg0H7UU0EFOPsmURbM1qUMaxedLtpIcJBtt0XkToZUbMWCcF6GEgI2XMYj66+J6AGMM8Q1YOjpRls3V2ypAJOKMXKTZSqJmwKloTtlLEkEOY0hFk1rpJ0BRzxiipeQijKkHr1xXVBEsFPBIRU11JM8Mnt1i7LubODyPpQa9UohVSlvI6PUsr2dzEH4bM9VUE2wuYNjwXtE4tEe2zh50+CEQOS6zDIxCK1Cp1Ui37wXgbDqg10OxW9H2hCfi5cPG0yFUo6TQfjUVBrkqFmGYPqhbd8aLRar8rrSNLB71ZKhZgwdzS0tLRCo0T30m234VwDoyYRNCEDT4jChdnpkq1eJy+SPz1QQ3w019bps/Zib5S1pyJhSuqwai3uTbOS/F+OHV4l1Y4YlUXm/ZGhKWqIfZ8cICJh5EoHCShhGGaRqciAYB+qmd3QLzso5vhsj3qvMd5V/bFu9oJY6cD4UKlV/+iyWAf7gNPFuJ/ZHvXeLOu3GeYVXqZIwsgjQ8xVXtbyg0ZT6LZyC/usdfwDh2U+GlmT8skVdEUKIV+74HLNnz3KzGUdLw3K9OgT3ZfxYR+8hCdF3VADcnA9RMxpUGRECvIPhMmwYmMMNvUHai7bDLqWM1KzphPfEGhgcu5T6OyecGpbVkBFkNvgL6SRMidBMr4Q8myFvDSo07sPqHrfBBqYSAiv15ue//28WnZLr6k9yU1rS017i6Tzj31zMvC/sB4zFtIJTU4V/eQaIQmqqWp/oBwsMXZPnbOwAD/jjCZOZCkBe67A56rXNsU8fnRmJjGXkpd602mQAlSriSjSovDQh68jaUtwNlNKplMo/IkziPLHV0e1A7LS0rg6SkJEqORVQQatRDCEZIggLXBac6WU33z5VS0svs2XPS5MrA2tvMJcBUIqAYNsJbzqs6ARMYrZrdYbBa/vfN87ZfZ1gS0cOnt6vARQZuS2ZExIDjQKVrBbwacHa+D2ex4O+h4lAqG0OYyibmVHfrlZ2o6JFwnCSoSCz/ZUoSE6l99VEOMnfkv/A/7zQUvkW7BgwYIFCxYsAPxfgPbOBDyqKsvjp5KqpJIiSyUmLAEJIN0IookN0qhoAu2Ctg2MSo/LjEFobWlFoF2aHhVQRxqHTxYdcPykk55pWwQcQLoFV2K7wWAPEYHWUSFq2IIhG5XKVql55+ZVqHq1vVf1lvuqzv/73ldLXiqV+979vf8599z7LNQE5pO4pFIx9K6icxH01piWcfQVa8UNI65vxMcarcv2SASHRO70vk5+pdjxy5Lg324So3cWxYtTb0gEh6QEQLHY6acJ23RqEVkuBEd7tsW78ACJ4MCTC/ANmJRQi2jiOBAafyBoEBx4dwMVwnaHmAsgGScfMLZSUxAcjIBBBTkCU6kKepe4qKWmIDioHR7MF2GQSy2SEKoWYUHhCMEhJmewmEIEchYEB4IBQgDvLVRGrZH0ahJBsSrZG8KSxEDAkYSV5A5IUcQmzSVjAZclyYAwXwwXKHdAikXVIiiSIvywJAEQposhAwGBpDYoZiSyo7AkKBBwiHELhQwknbQqlttLEhz0A0KumEOooHOVZJCaxLAjIQqwLAkAhTLRJVDYQOJJpk9kWkwMhUpyCSSTuIlyM842tZgMCOgOdgGVLZPMKXQSVQQHdaFQIkKBQgdSIqhKi1uZJhUcCAokggTBgaCgkRzNp6D/d4eg8NuD4Kz/Rthq4/5MV04BNBYWw8kho6H+3NHsOSnxIGEhKCQOBIYfeE/Yqtlzo9SZ7oC6kePgyNgyBg+SeSFh4QQKlGiMAQYTd6wVHMEh7r8rOo3PLrsZDl9wJR04eeIicWnhAAw0JClTg7/cCz9+fR2kdbhM/X8gKD677CY6oJFl+BCoxUAo4JyHLXQORBaCYMrLj6uSK+BNGIK8c8tjlLOIrBoREk0JDwcxhNgHNO8haaEQSn+d8QDUjRxPBz68Fui9xoRFZzAsgd4p06QIumLLChZCJJvQSbz2y2fZIylsqFGq15Rxi05QQLdwBGgUIqIwyTi16mHT5xTi1e7r5lLyMrJwpaolpoeDuMDKSjqe0cEw7T/upYYQRUlLWS5imJa5iBSNwbCPwCAvv4COgXRWYz/cxOo2SGGFLrxRTOybxzmIxUz76Pgldo5h4KAicObls8dhI0ZCbl4eOJ15AfscP3YUPnq/Gv53756Y/sbm+39POYjo2io4iBncw4HCCGXCq+OPX1/LbecffcGFrOMPG3Fe3J/X7nbDq6+8BIcO7Jf9O5h7wBwESf8ww6IyGLBugW4Oq0AYThg5XOkUrvbY+c8fM5aBQC+9VPWibEhsu/s5VmVJkqVStQqnLCqCAcMIKn9WIJwQNeXlpboCYLjwaM/I4OL/x5DjxXVrmKOIJEpOKpYq5dcWFaBAw5QxauyHm1niTQ37P7BoMLP+CAFp3M+7Xlz3LBz5+suwP8cKyh0Vy+mEUaa4hzstBAbj9JMNS2VNnMIr/cXjJrDOr6f111OYi4iUtPzTQ6/QCaNccc3ytMYBhmIRDKQYFWlq9cXjJ8D1P/sHbkIArXXjz29jj+EAgSEYTQFXrAqhn0KsgEghMPAHhzn3zGOdJVnA4A8IzI+QVAdEpS5w8Js4RdJAU66eqsqwoVl1W8Uv6CTgBBCxOAfKMWioyQIcklm+2gqSJoBYohkchA8nMKgoXH/RX5dOKqNGCdMOjYVDqWHi12KhD1eoDgfRlhRT+6qnM9mBhT10xexVqLCKSqhVU6U4vUEdOIgl0RXUrupKugJSLiXj/AAxMqzDIsWtXWLuMD44iJShuRIqKdvihU8L6uFI4UmoLBkQ8DOzFS/ppVtGDWXthdvV6R3UIPELwbAlbjhA76rQJJV0Z2YbAwSKVTYKGymyzvcLt2YJ7UdSRWViRBAbHMQ8AyUgNRTlGaKHFwRQzbRSrFlSBgdxEQnKM6is3V22gNc0QhFZF4+/JOB1nSeVGkVdbYnFOVCeQQs4dKYFvGbzJsZPYM+PfP0VNZCo48fq+uaU+OvVdjs1jroqCRdepIRxDbhzMbWbNvp9W2bAa1/hU2NjAzWOKJzGPUVSEIauQQpXkjrhRag3LWHg4KX20lY4YuFLTJLk6e7mXHizI50aQhutOnrthQsiOgelJZak2PRgSw41ggJtbs8gMGir+dLah1BhBd10Rgfhib7a1Y8aQoZ2d6UJMM2mhtABEGHhEG3ck6Syj3M54IkzWdQQUcBwS6OTGkIfLQ6bc6B1II0TVv7daHfDaGs3DE71JG07tHgtcKjbBm8JzmqzO4O9JumqvvUnLX5gKAOqhuRCyZyspKSj4aoW4FAuDSumUbvwoWRNVlLSkQuV+aom/eFA95vgRNhBpLUQiS6sYaCkIzea3gcHceZlMbUJP8JEJSbjkkW3NFHSkSNN83cOZdQeHHaYRmdSzCWY1HAOzZngLLTwh8NF1B78dpxD3VYCA0lX4QCFDw4UUnCs60/nJ1yIgUOUF50qJDDwq1wKK0wUYmA2PxGEQJj0fQHVMPCtkhRqA/MIs/lmL7lmYBBCCQID97ooRSx+IplEZi65xtAIwUAyV1hBMpGwBsJsQ38YEtEcCXOJ4GBS4aInmNAzgz3HkmgqcCI4kHSUL+Pf08bpcu09Xug60UAl0QQHklHytJxhndDb1c3Pd2rq/U4pdgKDWWWlJjA5GHoAbAPyGRi6G5p7D2peNljSbIY4he7GFvB2dIHFZmXfC1KEsKeejhPBgaSbWt1CR/Scndbt64wICbxqMxchdMxURwakCBvrpBrI29kFntY2BgRmRTPtYB10dlbpB+02aDzT0/szwadmpFkgzUrDmKaAw9FrL6wu2rmfWsIk8go8aG7rYY+hhJCwFuQGdFzcAvZJTRX2S2X7MgmPljDwYJARHIFXsChej6cPAgGxqQAfqzM7KoB6BEa42r3gAi9YhF1zMlPYI4lLvUfOwWRqcQeCAa/E4YShhTU/J+hKjx0cO32P+DyaECZgFTqy1QopGXawpNuE9+Slqy63d4GzXwr7zq4OL3R1e/sg1+TqYd/fnkaE4DmsqAYqoeZebULn6uk5+zrNprxjITCMyEegQ+hntwhQsAQ4H3dnr4tItxEgOFO1D//vUVvwH050dAXGEo5083UoXzgRBD26hQdXwnSDv3OgJek504C0VCjtlwalWTb4uKET/tt1NndQdmQf3PnxNhh24luA55Zz/X+89nhFL8zuWwhpk69iz58+3AL/edTVt0+nAD4KL7hRTV9YQUlJ4zQxzwFDMtJgsLBNdOJzG3ve1tEBJ5ub+/bbWBeYVKzY93YvGLBjnW6EtDw+S5Pxu6FsEyb2gQFVnm8PgMO4HAfsvGJE0O9/fNoFB1vdcKi1HQ62uIXn7XTS6BBS+OccKO+gsrKtqazj4zYmK4M9KupU3YEFTXubO/ueZ1lT4OJFD0Pz3b1X5C8e+x0M/qeZ4JzwI67a4PtdH8DxV7eLruHXAT8bnyNvfQpfG0YSAuPj02cYSHBr6fbQCRiftknhsI3goExjsuzCidsPrinMVtz5o8nd1RUSDKhRDiukFOYzm+569hn2Xt1/bYTjm7fDwJtuMBwS/lDwhRMWR+T2ef/UmbiOA25zhgbP+Kxzd8LO+hbYdLSRXIeCfIMUDlUQ5m67yS60/QiAOUPzmeXXQ92es1e/z88EDjeOz+0tSUab3l17GDq2b2WvPW43gwRuaflOKLzuKl1AgX+3YdeHDAr43F/4Hf3DCb2FxwuhIQUHggKBsVHYyGkEaJXvidWPFk1FO/fjWZb0S9SjC1g4or/qbiBWOLR6vEHOwafMO+8Gz5HD0H0gMGfU2dDYB4q+jioAwzFyuLCNYDkKfK4EAO11x8Fddwzajx6Dlk8PBYFAKnQL6BrkaFKBvovYMLcxaiAsETafEBbPfF0P37k7kz6kkDoH1OpkhAPmBxaeVxjSlhqh9q5Ap7C3KXDWJeYcAl4/sRyabr8JvC5X5DyGAIzOhr9B4+6/6fJ/SPMM4XIovOjmIifbUOgmFnxWB28IIUkSqdoXUqBSQsQa1cmUMzg0ZTTbeAEDg0NnYMf53BWYnAyVzMt7agUMznFAXkY6pBhYk+xIs8LArEzof/NMNkIhV/5uiJcLxvrSoVB3zVj2iK+TQKv9X1jD7FCW6K2wafxwQ8OGiBZeMnGitbsnIhhQPecWQ8eceyHzxecgM+3sYe3y9ICrqxs6hCshPldLCKAMWyrYrVb26C/vOYXQfuusiL8vdQ42r4eN0KRZ+avox3wTXkBm7/smkZ1EjWAOtkaEA+5QtHN/dSIDAq8EvIKB2X+/YUwl9rv78jJI/fwgWD84ez9kW2oK5Kbqu6x9+7yHou5zrD3YDWFtB49w8D9vbt57mA2XJqCWBl0Awuy4IFHBgOEDXgl4ln9YEW6kIpw65vyKuQjDwCY4Bjl//2i7JyisaG3nf6gRAZGguYatsuAg7Ijlk1WJ2Ar+2WneXQMLKSQjFVmp0fMJ7b9ZCt5M/Z0RQqHr6utl7St1RJhkxREaaTKWN2HugfdzKAaFjAHDzrsVAIG/0JRILbDwvP7cf8ceb+QZSKP6RZ9RiWBAQOjueOY9LGu/YxLX4J9HaY0yPMqL+0ygBOUqoa/XKoJDJKKYFg4jCrn/jtKRiniu4p236nf48G95zymQte/RDk9Y4J1pN0cV45zihLj/Rq0AhrAphIhwEOOQhAgvZhaZ454JbhVtNVr87svLNf/OnosvkR1OhAopRjkC3RAmJvl3D/mJ0C0inhxRl/MRw4tas7fCzYPMAYeenh5FHSuaMEGJQ4taCv+GEm07GTjDdHJ+YJK12QShhW9inZnzDOHCCdlwkEMYM8gsB1KakJTWNUhHL2S5kcf/TUMw3Kso+Ylw8x+pwP9PWvGJoVW03AsP4n3UK4KqBDBEjQhkwUEkzAyztoRZDmKoTD12npSOdsioq4Xcfbvh4IaX4d3qjxR9rlYJSgxZsLYiHtcwvX9myP1a2tq4P17XmhMOtWI0AKrAwS//YMr6B7McxM4QcHC52mD488ug6NVKOOevOyDrUA1s2LhdeV5g1Bjomj5Tte+KwFEaTuAoxdaTZ0OGInsqTOufEXJflwnyDjjjE0vwTSQcfSyVu7Oi8ZjWP67bnX37PTjQXmamFlk5djCkp/B/cy+3YKel7iEtzQb1pxrgSO13fsBwg8ORCT/8wXDFgEj9thZSjh+NP5y4ZwF4BxbJ3h8hN/uf74O8PdWQ/fcaSP/+BNxe5ICSYaE/w9PTA470dEjl/Lid6vSYqWLyfOEif0LuzopbXvjwJWCiEQxci8EsY9LhRirm/aqCwcBf6ytfgQMH/0956DLvobgTlDgygSMUSrTgwSf6nltbmpgDGmKJnD8xw6gFLu1nEg2LloCMGw4iIGaZBRBmyihHqnFYueLRoPceWbwiJkC448g/IFiU1k/cNXcR1Nc3BLw3ufxSmFx2acTfazZB3mFMtj0hwRAzHMwECL1WbopX/ou7hFJhQT48ufSBkIBAF6GsgxfImhwVznnIFYZC02+6KyQY0A1FE45Y+MIsr+sMdH1WwzbPyRPcHDd0pUP4PsdiAgMq7on/RTv3Y5jB7bL2PE/NDuh0Qic43tgoK3a/a+5v2aO/MOx4cumvYVjxENl/M+1PlWB78y/yw4npM6FTZlJz2dNrYc//1AS9P3vWz+GG66dEPzG/PwX2Ncsh5dvI57XF0Q9yH3sKbGNLDDt2HM/UjBkMqsCBd0DgYh1mUJPLBY0u+SfYmn+vgnd3BQ9pKoWE/XeL2TTvqFfxc4vB/fiKqOBaX7Ux7Pd6Ye1TQbmTeL6TFBLnCH8bH/UWLi33zFcnuTqdRDDENTdKtSWDBECUCQ+7uIoHs+zwxqUjIRGFlruxuQXuWbgEvvjqSMh9LhjzQ5h3bwULSSIpc+4dYGmLDKa2FevCzp3Y/pd32PCq1M30hT4PzoWrJl/W61bwfpviCAQuGCNdv6HlmWXQ/vaOkJ/jXL6GOQTXS5Vskyq1/wDIr9yo+7FA14DugRPhoi2lanyQquuJCYAoFh72CVsuD62E4QSGFYmu1jMuWPTkSnj/o08i7oex/oTxJTDhkpIgC5/5wD1hfw8TkL65EwiAPXtrYPuf3wkYXg3lFBY9NFcA1A/AmpoK/XNyZC3kUn/dFYGfc9sstoW9RP7mfujcvy8IIHoKV7K+5qMveTgVquQWOOkOBz9I4NEqMbqlcIq2GWZixiPpnbGww655ripix42ki1J7y7c/9cS2ItM/zryBbeGUnZkJ+f3CW//T994J3Ye/isk5oAo2vW5IaDH4jc+MPhVmySmJNhwOvOQhVl4wuG814UQTzsHABGa0OQhYah3tKh+PfEOS6BCUKJybwJGIhlmhk5797roPbCNGgvutHSFDj+yFi8D+k6mGHA8Dk5Ks6jGexKPucBABUSKGGYbILCMVSnMNCAXpBC0lwtDgSG0dHDj4BYOHdKjRp7HDBrMy6cLCfJa3wByGUghEktPhgNwwd8KK5CCCQDP8POYqjHAMBsMBl3fTbFKkLmuYC5DARGWZ3i1nlpEKpfrm1CnVZi3edsf8oETim4UdrAJS6dwJpRpaUBB1GX10Eu5tm1hewQcLhIF1+EjIuGqqoUOY/jJgxGJGqHUf1ZQuS/0i3QRA4M1ytgApbuUIcbuSYc9IDkIKBnQH7bNv1HyRWsw9yLm/Bo5AYDhB6hMWj5THO0zJDRxEQCDlLHolKxMtnPAXWnE14IChhVQYNuixenVORkZCHROd5lho7hYMgYMfJEqNzkXEq+crN8AfN21nQ4jhNK70Anhi0TwYNECb0RK88sa75gHmHKRSUmEZq/rZ7SwhSeIjt8ANHERA1IguAu/qPV+LvzEmS90rE4Jg9rxHwhYcSfXJvgMwdeZd7Pmqf10E5ZMmqH7ljRcOoUYw5FQwqhEW6anTDachLz/PjE61SQwhaozop4beXghXvhUAgVMEd6kdamTb1LsyHTtR39fRA2L2EaPg9IRy6CgYwF7jik25Nbshd9/H7LlP8/9lGfxs6mTmJFQ7cMKVF6/A8azWjOtChAortJQ9Lc2Qu1rpAQiVpXrdglIZvpIGJlbEcs9S4PQ+GY8uWxPwuifdDod/uQiO//SWPjD43j89oYz9rHV0IOte2/EufFJzgKsrsDSswGSk1nI69M8FNQhg0EMqzc7EKkeL0WDgAg7+oYawOdWChJoJIgwRArxe6UQGgkhqOb806ufEK7wC45U4FuHqUnoLv6vdZjPk/NIDEIMzbGpAgZt7xXC3BpfakFBD0qRi9qHouVTH4b8HvYdJStXj9xiz/qEKn7QOKRJthEIlYT7ByRMUuIUDj5B44reBuQJc5uy81Yuh/1tbwNbSJAFHDVsQFleK9hfmHMaVqA+HzPT0mGL4UCMVWAmplTBHgt/VKLW0tGr+NxQmwX1OoVSPmoWYjhnvWBUztU5x+BMTl7JmfKqZPcZO/cHrL7GkpP/wJa6DiFtUuCyax+Cg2RU5MxNOtbTE/TmFBdrd4s2IXINPp3XKOchMglfx6BJMCYcQkMgFDUY3oimrn4MBAuGwrnIDvLRpe9RQBB2HFm5BKhy1aGhtVVRSHWrtSa3CCqyExO9opNqMv4vWLB6SjAkJBz9I9K29b8TMT4TEQ/fNhhnXXAk3zXkgKKewfvWThrSL0pLq+lPf6/bdjHQNPrnbDIFDLfRWNdaACWUFE0tcJn+JGHLgvI3i3tjPDskmpSXV0oSklsOY2ToXPRklvxEy04QOCQsHScgxzOcmhNhPczcRKo49drze0HaQW1IdKqTQKhnJg2vwDWNqXQg1Mc9hSJmzVkqBBBO6ic2XjMDpfniQNM0CDz93UCAcThgLB7lDhSFHKgq0gUOyuAZBOCFqRiL9QymJeqQsFku1sDmFDUGxQG1QhCuqMRIQcocLQzkH6bqSaoFBzrRsvR2EimKO1dKrGcLWRHAwHyhW+YFCtdtNDx8yKOg9tasgFbuHKFdqrIyUOgecianFbEweip66urq1AEKpCAR8rE3UfpMUcJCAYol4YGN2FL6CmmHnBsPh3Q/2GPr/YXlypJLq7X9+O+g9XAdSbaGD4WFadqtf/Ucc9Q7Vfg4BgVCTDH0l6eAQwVGUiydBVHWLt2hD5+DMyQr42a7390Rc58HI3AO6BrzHhL9wiracO1ApFQ+JyDiFDtMpAqE8kR0CwUFejqJcBIVTPDmaolnVy350YdDPn352vaH/S7ir9rLla4Pek3PPSsXuxaBp2XHmHPDCUG45qyWJlkMgOKgDiibx5HCGgkWLn1W9bNzYoIlZOD0bHQRP7gFvnydd3IXd5EaDRCRPriFCZaQUBvi8ms5+v35ATaBcXq8XS7jxknu/sBXj6lAz71wQtN+OjS9otkycXOENeh99ag28+c77Ae9jXcMLa5ep9newPBqhwPHybzjUuJoAQHDQXQ889nTJm7s+RFjgKtt9k8PWr3lSl/kV4TT7/keCRlDUAAOGDggDo9ZniCJMGK5GICR7aEBw4FQXTppWjA7j1huvv+Ph+39RrOffxlWncM1Lqfznf+BELayobBa2aJO20BEgDIyeQBXGEWwTIFBFZxzBIRHCEgz0y4TtDlB5dimGOAiFUCMm0aaOd3s8bH4GrkuJxUtYM8FJIVOTCIH3yA0QHJIZHMUiOK4UH6M6DqzKxHUuwxVgGTlbVIGqxc6PIUE1AYDgQIoNILnPV24oWSdswsuhovsoloIEXQJOL8dp5gZ2eOzkn4qdvjZZCohIJBKJRCKRSCQSiUQikUih9f9AwRVcXTx0WQAAAABJRU5ErkJggg=="
                      alt=""
                      width={80}
                      height={78}
                    />
                  </span>
                </div>
                <div className="col-md-10 col-xs-11">
                  <h1>{DOCTOR_FULL_NAME}</h1>
                  <h2>{`${medecin.specialities[0].name}`}</h2>
                  <a className="btn btn-primary">Prendre Rendez-vous</a>
                </div>
              </div>
              <div className="clearfix"> </div>
              <div className="row"> </div>
            </div>
            <div
              className="alert alert-info col-sm-12"
              style={{ marginTop: "25px" }}
            >
              Vous êtes <strong>{DOCTOR_FULL_NAME}</strong>
              <br />
              <a
                style={{ color: "#00aec5 !important" }}
                href="/contactez-nous/#5e05d54883933f2acc1b6647"
              >
                Activer votre compte
              </a>
              et gérer votre cabinet 🎉<strong>Gratuitement</strong> 🎉 en
              mettant à jour votre profil .
            </div>
            <h2 className="choose">Choisissez un rendez-vous</h2>
            <div className="pick-date-profil row">
              <div className="col-md-12 steps">
                <span className="number-step">1</span>
                <div className="row">
                  <p className="col-md-6 motif">
                    Quel est le motif de votre visite ?
                  </p>
                  <form className="col-md-6 ng-pristine ng-valid">
                    <div className="form-group">
                      <select className="form-control ng-pristine ng-untouched ng-valid">
                        <option value="5e05d54883933f2acc1b664b">
                          Simple consultation
                        </option>
                      </select>
                    </div>
                  </form>
                </div>
                <div className="clearfix"> </div>
                <span className="number-step">2</span>
                <div className="row">
                  <p className="col-md-12 motif">
                    Choisissez un rendez-vous qui vous convient dans le
                    calendrier.
                  </p>
                </div>
              </div>
              <div className="clearfix"> </div>
              <div className="col-md-12">
                <div></div>
              </div>
              <div className="clearfix"> </div>
              <div className="ng-scope">
                <div className="doctor-bloc-calendar">
                  <div style={{ textAlign: "center", paddingTop: "51px" }}>
                    Il n'y a pas de temps disponible cette semaines
                  </div>
                  <div className="clearfix "> </div>
                  <div className="col-md-12"> </div>
                </div>
              </div>
              <div id="rendez-vous" className="clearfix"></div>
            </div>
          </div>
          <div className="col-md-4">
            <div
              id="mapid"
              className="map leaflet-container leaflet-touch leaflet-fade-anim leaflet-touch-zoom"
            >
              <MapWithNoSSR
                center={[medecin.address.lat, medecin.address.long]}
                coordinates={[
                  {
                    markerName: ` Dr. ${medecin.name} ${medecin.lastname}`,
                    position: [medecin.address.lat, medecin.address.long],
                  },
                ]}
              />
            </div>

            <div className="grey info-sup">
              <a
                class="btn btn-primary"
                href={`https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${medecin.address.lat},${medecin.address.long}`}
                target="_blank"
              >
                Ouvrir dans Maps
              </a>{" "}
              {!displayPhones && (
                <a
                  onClick={() => setDisplayPhones(true)}
                  className="btn btn-primary"
                >
                  Afficher le numéro
                </a>
              )}
              {displayPhones && (
                <div>
                  <div>Téléphone(s) :</div>
                  {Array.isArray(cabin_phone) &&
                    cabin_phone.map((phone) => (
                      <a href={`tel:${phone.number.replace("tel:", "")}`}>
                        {phone.number.replace("tel:", "")}
                      </a>
                    ))}
                </div>
              )}
              <div className="clearfix"> </div>
            </div>
          </div>
        </div>
        <div class="container ">
          <div class="row">
            <div class="col-md-8 search_in_profile">
              <h4>Vous souhaitez rechercher un autre médecin ?</h4>
              <SearchBar />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { slug, id } = ctx.params;
  const handler = nc();
  handler.use(all);
  await handler.run(ctx.req, ctx.res);

  const medecin = await getDoctor({
    query: {
      id,
    },
  });

  return {
    props: { medecin },
  };
}
// export const config = { amp: 'hybrid' }

export default MedecinProfilePage;
