import { signIn, signOut, useSession } from "next-auth/client";

const IndexPage = () => {
  const [session, loading] = useSession();

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <div className="container" style={{ margin: "auto" }}>
        {session && (
          <div>
            {JSON.stringify(session)}
            Hello, {session.user.email ?? session.user.name} <br />
            <button onClick={() => signOut()}>Sign out</button>
          </div>
        )}
        {!session && (
          <div>
            You are not logged in! <br />
            <button onClick={() => signIn()}>Sign in</button>
          </div>
        )}
      </div>
    </>
  );
};

export default IndexPage;
