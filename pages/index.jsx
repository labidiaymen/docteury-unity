import React from "react";
import { getDoctors } from "@/db/doctor";
import { HomeSlide } from "@/components/Layout/Default/components/HomeSlide";
import { MediaBox } from "@/components/Layout/Default/components/MediaBox";
import Head from "next/head";

const IndexPage = ({ doctors }) => {
  return (
    <>
      <Head>
        <meta
          name="description"
          content="Docteury.tn est une plateforme qui permet de trouver des médecins et d'effectuer des rendez-vous médicaux en ligne."
        />
        <meta
          name="keywords"
          content="medecin, médecin, en ligne, rendez-vous, rendez vous, Prenez, tunisie, tunis, docteury, docteury.tn"
        />
      </Head>

      <HomeSlide />
      <MediaBox />
    </>
  );
};

export async function getServerSideProps(ctx) {
  const doctors = await getDoctors({
    query: {
      city: "ariana",
      speciality: "generaliste",
    },
  });

  return {
    props: { doctors },
  };
}

export default IndexPage;
