// const { getAllDoctors } = require("../db/doctor");
// const fs = require("fs");
// const globby = require("globby");

// (async () => {
//   console.info("Generating Sitemap");
//   const pages = await globby(["pages/*.js", "!pages/_*.js"]);
//   const doctor = await getAllDoctors();
//   const sitemap = `
//         <?xml version="1.0" encoding="UTF-8"?>
//         <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
//             ${pages
//               .map((page) => {
//                 const path = page
//                   .replace("pages/", "/")
//                   .replace("public/", "/")
//                   .replace(".tsx", "")
//                   .replace("/index.xml", "");
//                 const route = path === "/index" ? "" : path;
//                 return `
//                         <url>
//                             <loc>${`${siteMetadata.siteUrl}${route}`}</loc>
//                         </url>
//                     `;
//               })
//               .join("")}
//         </urlset>
//     `;
//   // eslint-disable-next-line no-sync
//   fs.writeFileSync("public/sitemap.xml", sitemap);
//   console.info("Success generation of sitemap 🎉");
// })();
