import prisma from "../lib/prisma";

export async function getCities() {
  return prisma.city.findMany({});
}

export async function getCity(slug) {
  return prisma.city.findUnique({
    where: {
      slug,
    },
  });
}
export async function getSpeciality(slug) {
  return prisma.speciality.findUnique({
    where: {
      slug,
    },
  });
}

export async function getSpecialites() {
  return prisma.speciality.findMany({});
}
