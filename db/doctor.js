import prisma from "../lib/prisma";

export async function getDoctors({ query }) {
  const { page = 0 } = query;
  return prisma.doctor.findMany({
    where: {
      specialities: {
        every: { slug: query.speciality },
      },
      City: {
        is: { slug: query.city },
      },
    },
    include: {
      specialities: true,
      City: true,
      address: true,
    },
    skip: page * 6,
    take: 6,
  });
}

export async function getAllDoctors() {
  return prisma.doctor.findMany({
    where: {
      specialities: { some: {} },
    },
    include: {
      specialities: true,
      City: true,
      address: {
        include: {
          cabin_phone: true,
        },
      },
    },
  });
}
export async function getSearchDoctors({ query }) {
  const { page = 0, keywords } = query;
  // const doctors_ids = []
  const doctors_ids = await prisma.$queryRaw`
    SELECT * FROM "Doctor"
    WHERE to_tsvector('simple', concat_ws(' ', name, lastname, 'dr')) @@ plainto_tsquery('simple', ${keywords})
  `;

  return prisma.doctor.findMany({
    where: {
      id: {
        in: doctors_ids.map((medecin) => medecin.id),
      },
      specialities: { some: {} },
    },
    include: {
      specialities: true,
      City: true,
      address: true,
    },
    skip: page * 6,
    take: 6,
  });
}

export async function getDoctor({ query }) {
  const { id } = query;
  return prisma.doctor.findUnique({
    where: {
      id: parseInt(id),
    },
    include: {
      specialities: true,
      City: true,
      address: {
        include: {
          cabin_phone: true,
        },
      },
    },
  });
}

export async function insertDoctor(entry) {
  return prisma.doctor.create({
    data: {
      ...entry,
    },
  });
}

export async function updateSingleDoctor(where, entry) {
  return prisma.doctor.update({
    where,
    data: {
      ...entry,
    },
  });
}

export async function updateDoctor(entry) {
  const { id, slug = "", city = null } = entry;
  const query = {
    where: { id },
    data: {
      specialities: {
        set: { slug },
      },
    },
  };
  if (city != null) {
    query.data.City = {
      connect: {
        slug: city,
      },
    };
  }
  return await prisma.doctor.update(query);
}

export async function syncDoctors() {
  return prisma.doctor.findMany({
    where: {
      specialities: {
        every: { slug: "allergologue" },
      },
    },
    include: {
      specialities: true,
      City: true,
    },
    take: 2000,
  });
}
