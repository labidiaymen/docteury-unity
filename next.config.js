require('dotenv').config();

module.exports = {
  async redirects() {
    return [
      {
        source: '/medecin/psychiatre/sousse/dr-frikha-ameur-5e05f26e65cb3e07e0898fdf.html',
        destination: '/medecin/profile/dr-frikha-ameur/57294',
        permanent: true,
      },
      {
        source: '/medecin/ophtalmologue/le-kef/dr-sfina-hamza-5e05f1ab65cb3e07e0898e04.html',
        destination: '/medecin/profile/dr-sfina-hamza/57199',
        permanent: true,
      },
      {
        source: '/medecin/gastro-enterologue/tunis/pr-riadh-bouali-mohamed-5e05f01065cb3e07e08989fe.html',
        destination: '/medecin/profile/dr-riadh-bouali-mohamed/56993',
        permanent: true,
      },
    ]
  },
  env: {
    MONGODB_URI: process.env.MONGODB_URI,
    CLOUDINARY_URL: process.env.CLOUDINARY_URL,
    DB_NAME: process.env.DB_NAME,
    WEB_URI: process.env.WEB_URI,
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
    EMAIL_FROM: process.env.EMAIL_FROM,
    SESSION_SECRET: process.env.SESSION_SECRET,
  },
};
