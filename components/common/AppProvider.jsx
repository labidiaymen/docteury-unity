import { createContext, useContext, useState } from "react";

export const AppContext = createContext();

export function AppWrapper({ children }) {
  const [cities, setCities] = useState([]);
  const [specialities, setSpecialities] = useState([]);

  return (
    <AppContext.Provider
      value={[cities, setCities, specialities, setSpecialities]}
    >
      {children}
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}
