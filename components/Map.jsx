import { MapContainer, TileLayer, Marker, Popup, icon } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css";
import "leaflet-defaulticon-compatibility";
import * as L from "leaflet";

import { useEffect, useState } from "react";
const LeafIcon = L.Icon.extend({
  options: {},
});
const Map = ({ coordinates, center }) => {
  const [defaultZoom, setDefaultZoom] = useState(12);

  const [defaultMapCenter, setDefaultMapCenter] = useState([36.8065, 10.1815]);
  useEffect(() => {
    let firstDoctorInList = coordinates.find(
      (coordinate) => coordinate.position && coordinate.position[0]
    );

    if (firstDoctorInList) {
      const [lat, long] = firstDoctorInList.position;
      const [defaultLat, defaultLong] = defaultMapCenter;

      if (
        lat &&
        long &&
        parseFloat(lat) !== defaultLat &&
        parseFloat(long) !== defaultLong
      ) {
        setDefaultMapCenter([parseFloat(lat), parseFloat(long)]);
      }
    }
  }, []);

  return (
    <MapContainer
      center={center && center[0] ? center : defaultMapCenter}
      zoom={defaultZoom}
      scrollWheelZoom={false}
      style={{ height: "100%", width: "100%" }}
    >
      <TileLayer
        url={`https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibGFiaWRpYXltZW4iLCJhIjoiY2phODVxazQ0MDQwdTJ4cW1mNWc1ZW44YSJ9.fLAg_X_X8U5e_7A_gvbg8Q`}
        attribution='Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery &copy; <a href="https://www.mapbox.com/">Mapbox</a>'
      />
      {coordinates &&
        coordinates.map((coordinate) => {
          if (coordinate && coordinate.position && coordinate.position[0]) {
            return (
              <Marker
                position={coordinate.position}
                icon={
                  new LeafIcon({
                    iconUrl: "/images/pin.png",
                  })
                }
              >
                <Popup>
                  {coordinate.markerName}
                </Popup>
              </Marker>
            );
          }
        })}
    </MapContainer>
  );
};

export default Map;
