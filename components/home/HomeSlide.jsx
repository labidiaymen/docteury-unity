export default function HomeSlide() {
  return (
    <>
      <div className="container-fluid slide">
        <div className="container">
          <div className="row">
            <div className="col-md-5 center-slide-text">
              <h1>Trouvez le bon médecin</h1>
              <h2 >
                Prenez facilement votre rendez-vous en ligne
              </h2>
            </div>
          </div>
          <form
            className="form-inline form-big-research"
            action="/patient/search"
            noValidate
            name="indexForm"
            method="GET"
          >
            <div className="form-group col-md-3 padding-left-0">
              <input
                type="text"
                className="form-control form-search"
                id="searchmedecin"
                name="searchmedecin"
                placeholder="Chercher un médecin"
                autoComplete="off"
              />{" "}
              <span className="or">OU</span>
            </div>
            <div className="form-search-type col-md-6">
              <div className="row">
                <section className="form-group col-md-6"></section>
                <section className="form-group col-md-6"></section>
              </div>
            </div>
            <div className="form-group col-md-3 padding-right-0">
              <a className="btn btn-danger" id="btn-search">
                Rechercher
              </a>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
