import React from "react";
import Head from "next/head";
import Link from "next/link";
import { useCurrentUser } from "@/hooks/index";
import SideBar from "../SideBar";
import DefaultHeader from "../DefaultHeader";
import { NavBar } from "./components/NavBar";
import { DefaultLayoutFooter } from "./components/Footer";

export default function DefaultLayout({ children }) {
  return (
    <>
      <Head>
        <title>Docteury</title>
        <meta
          key="viewport"
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta name="description" content="" />
        <meta property="og:title" content="Docteury" />
        <meta property="og:description" content="" />
        <meta property="og:image" content="https://" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
      </Head>
      {/* <DefaultHeader /> */}
      <NavBar />

      {children}
      <DefaultLayoutFooter />
    </>
  );
}
