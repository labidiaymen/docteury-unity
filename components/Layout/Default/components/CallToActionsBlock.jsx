export const CallToActionBlock = function () {
  return (
    <div className="container-fluid services-doctor">
      <div className="container">
        <div className="row">
          <div className="col-md-1">
            <img alt="" src="/images/med.png" />
          </div>
          <div className="col-md-8">
            <h4>Vous êtes professionnel(le) de santé ?</h4>
          </div>
          <a className="btn btn-danger col-md-3" href="/nos-services">
            Découvrez Docteury
          </a>
        </div>
      </div>
    </div>
  );
};
