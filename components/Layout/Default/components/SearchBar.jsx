import { AppContext } from "@/components/common/AppProvider";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import Select from "react-select";

export const SearchBar = function ({ city, speciality, keywords }) {
  const router = useRouter();
  const [searchKeywords, setSearchKeywords] = useState(keywords);

  const [cities, setCities, specialities, setSpecialities] =
    useContext(AppContext);
  const [currentSpeciality, setCurrentSpeciality] = useState(speciality);
  const [currentCity, setCurrentCity] = useState(city);
  const cities_options = cities.map((city) => ({
    value: city.slug,
    label: city.name,
  }));
  const specialities_options = (specialities || []).map((speciality) => ({
    value: speciality.slug,
    label: speciality.name,
  }));

  useEffect(() => {
    fetch("/api/common")
      .then((result) => result.json())
      .then(({ cities, specialities }) => {
        setCities(cities);
        setSpecialities(specialities);
      });
  }, []);

  const onSpecChange = ({ value }) => {
    setCurrentSpeciality(value);
  };

  const onCityChange = ({ value }) => {
    setCurrentCity(value);
  };

  const naviagteToHome = (e) => {
    e.preventDefault();
    if (searchKeywords) {
      router.push(`/search/${searchKeywords}`);
    } else if (currentSpeciality && currentCity) {
      router.push(`/medecin/${currentSpeciality}/${currentCity}`);
    }
  };
  const submitSearchForm = (e) => {
    e.preventDefault();
  };
  return (
    <form
      className="form-inline form-big-research "
      name="userForm"
      noValidate=""
      onSubmit={submitSearchForm}
    >
      <div className="row">
        <div className="form-group col-md-4">
          <input
            type="text"
            value={searchKeywords}
            className="form-control form-search "
            name="searchmedecin"
            autoComplete="off"
            onChange={(e) => setSearchKeywords(e.target.value)}
            placeholder="Chercher un médecin"
          />
          <span className="or">OU</span>
        </div>
        <div className="col-md-6">
          <div className="form-search-type">
            <div className="row">
              <section className="form-group col-md-6">
                <Select
                  instanceId="postType0"
                  value={specialities_options.filter(
                    ({ value }) => value === currentSpeciality
                  )}
                  onChange={onSpecChange}
                  className="search_select_input"
                  placeholder={"Spécialité"}
                  options={specialities_options}
                />
              </section>
              <section className="form-group col-md-6">
                <Select
                  instanceId="postType"
                  value={cities_options.filter(
                    ({ value }) => value === currentCity
                  )}
                  onChange={onCityChange}
                  className="search_select_input"
                  placeholder={"Ville"}
                  options={cities_options}
                />
              </section>
            </div>
          </div>
        </div>
        <div className="form-group col-md-2">
          <button
            type="submit"
            onClick={naviagteToHome}
            className="btn btn-success"
          >
            Rechercher
          </button>
        </div>
      </div>
    </form>
  );
};
