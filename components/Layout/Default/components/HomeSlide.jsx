import { SearchBar } from "./SearchBar";

export const HomeSlide = function () {
  return (
    <>
      <div className="container-fluid slide">
        <div className="container">
          <div className="row">
            <div className="col-md-5 center-slide-text">
              <h1>Trouvez le bon medecin</h1>
              <h2>Prenez facilement votre rendez-vous en ligne</h2>
            </div>
          </div>
          <SearchBar />
        </div>
      </div>{" "}
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="steps-for-index row">
              <div className="col-md-4 step-index">
                <span>
                  <img src="/images/medecin-ecoute.png" alt="" />
                  <em className="notif">1</em>
                </span>
                <h3>Un médecin à votre écoute</h3>
              </div>
              <div className="col-md-4 step-index">
                <span>
                  <img src="/images/un-rdv-a-lheur.png" alt="" />
                  <em className="notif">2</em>
                </span>
                <h3>Un rendez-vous à l'heure</h3>
              </div>
              <div className="col-md-4 step-index">
                <span>
                  <img src="/images/ne-expertise-medicale.png" alt="" />
                  <em className="notif">3</em>
                </span>
                <h3>Une expertise médicale</h3>
              </div>
            </div>
          </div>
        </div>
        <div className="clearfix"></div>
        <div className="row">
          <div className="col-md-12 main-title">
            <h2>Pourquoi choisir Docteury comme partenaire de santé ?</h2>
          </div>
        </div>
        <div className="clearfix"></div>
        <div className="row center-features">
          <div className="col-md-2 features">
            <img src="/images/ico-home-1-2.png" alt="" />
            <h3>Trouvez un médecin à votre disposition en cas d'urgence</h3>
          </div>
          <div className="col-md-2 features">
            <img src="/images/ico-home-2-2.png" alt="" />
            <h3>Trouvez un médecin compétent à deux pas de chez vous</h3>
          </div>
          <div className="col-md-2 features">
            <img src="/images/ico-home-3-2.png" alt="" />
            <h3>
              Prenez et modifiez vos rendez-vous oÃ¹ et quand vous voulez, 24/7
            </h3>
          </div>
          <div className="col-md-2 features">
            <img src="/images/ico-home-4-2.png" alt="" />
            <h3>Soyez informé de vos rendez-vous par un simple SMS</h3>
          </div>
          <div className="col-md-2 features">
            <img src="/images/ico-home-5-2.png" alt="" />
            <h3>Partagez votre dossier médical pour un meilleur suivi</h3>
          </div>
        </div>
        <div className="clearfix"></div>
      </div>
    </>
  );
};
