import Link from "next/link";

export const DefaultLayoutFooter = () => {
  return (
    <footer className="container-fluid soft-footer">
      <div className="container">
        <div className="row">
          <div>
            <div className="col-md-3">
              <h4>Recherches fréquentes</h4>
              <p>
                <Link href="/medecin/dentiste/tunis">
                  <a>Dentiste à Tunis</a>
                </Link>
                <Link href="/medecin/dentiste/ariana">
                  <a>Dentiste à Ariana</a>
                </Link>
                <Link href="/medecin/ophtalmologue/tunis">
                  <a>Ophtalmologue à Tunis</a>
                </Link>
              </p>
            </div>
            <div className="col-md-3">
              <h4>Docteury</h4>
              <p ng-controller="inscriptionController" className="ng-scope">
                <a
                  ng-click="inscription()"
                  onclick='ga("send","event","Footer","Inscription","footer")'
                >
                  Inscription
                </a>{" "}
                <a
                  data-toggle="collapse"
                  data-target="#bs-example-navbar-collapse-1"
                  ng-click="connexion()"
                >
                  Connexion
                </a>
                {/* <a
                  href="/a-propos"
                >
                  A propos
                </a>
                <a
                  href="/contactez-nous"
                >
                  Nous contacter
                </a> */}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid conf-footer">
        <div className="container">
          <a href="/cgu">Conditions Générales</a> -{" "}
          <a href="/confidentialite">Politique de confidentialité</a>
        </div>
      </div>
    </footer>
  );
};
