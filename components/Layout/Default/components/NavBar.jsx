import Image from "next/image";

export const NavBar = function () {
  return (
    <div className="top-nav">
      <div className="top-nav shadow">
        <nav className="navbar navbar-default">
          <div className="container-fluid">
            <a className="logo-small" href="/">
              <img src="/images/logo-docteury-d.png" alt="docteury logo" />
            </a>
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle collapsed"
                data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1"
              >
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>{" "}
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <div
                className="collapse navbar-collapse"
                id="bs-example-navbar-collapse-1"
              >
                <a className="logo-big" href="/">
                  <Image
                    src="/images/logo-docteury.png"
                    alt="Docteury"
                    width={160}
                    height={40}
                  />
                </a>
                <div></div>
                <ul>
                  <div className="nav navbar-nav">
                    <li>
                      <a
                        className="nav-only-device"
                        data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1"
                      >
                        Inscription
                      </a>
                    </li>
                    <li>
                      <a
                        className="nav-only-device"
                        data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1"
                      >
                        Connexion
                      </a>
                    </li>
                  </div>
                </ul>
              </div>
            </div>

            <div
              className="collapse navbar-collapse"
              id="bs-example-navbar-collapse-1"
            >
              <ul className="nav navbar-nav navbar-right">
                <div className="ng-scope">
                  <a className="btn btn-primary navbar-btn right">
                    Inscription
                  </a>
                  <a className="btn btn-strock-primary navbar-btn right">
                    {" "}
                    Connexion{" "}
                  </a>
                </div>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div className="clearfix"></div>
    </div>
  );
};
