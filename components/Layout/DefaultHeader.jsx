import React, { useEffect } from "react";
import Select from "react-select";
import { useContext, useState } from "react";
import { AppContext } from "../common/AppProvider";
import { useRouter } from "next/router";

function DefaultHeader() {
  const [state, setState] = useState([]);
  useEffect(() => {
    fetch("/api/common")
      .then((result) => result.json())
      .then(({ cities, specialities }) => {
        setCities(cities);
        setSpecialities(specialities);
      });
  }, []);

  const router = useRouter();
  const { pathname } = router;
  const [currentCity, setCurrentCity] = useState();
  const [currentSpec, setSpecCity] = useState();
  const [cities, setCities, specialities, setSpecialities] =
    useContext(AppContext);
  const cities_options = cities.map((city) => ({
    value: city.slug,
    label: city.name,
  }));
  const specialities_options = (specialities || []).map((speciality) => ({
    value: speciality.slug,
    label: speciality.name,
  }));

  const onCityChange = ({ value }) => {
    setCurrentCity(value);
  };
  const onSpecChange = ({ value }) => {
    setSpecCity(value);
  };
  const navigate = (e) => {
    e.preventDefault();
    router.push(`/doctor/${currentSpec}/${currentCity}`);
  };
  const naviagteToHome = (e) => {
    e.preventDefault();
    router.push(`/`);
  };
  //"hidden md:block"
  return (
    <>
      <div className="top-nav">
        <div className="top-nav shadow">
          <nav className="navbar navbar-default">
            <div className="container-fluid">
              <a className="logo-small" href="/">
                <img src="/images/logo-docteury-d.png" alt="docteury logo" />
              </a>
              <div className="navbar-header">
                {/* <button
                  type="button"
                  className="navbar-toggle "
                  data-toggle="collapse"
                  data-target="#bs-example-navbar-collapse-1"
                >
                  <span className="sr-only">Toggle navigation</span>{" "}
                  <span className="icon-bar"></span>qsdqs <span className="icon-bar"></span>{" "}
                  <span className="icon-bar"></span>
                </button> */}
                <div
                  className=" navbar-collapse"
                  id="bs-example-navbar-collapse-1"
                >
                  <a className="logo-big" href="/">
                    <img src="/images/logo-docteury.png" alt="docteury logo" />
                  </a>
                  <div></div>
                  <ul>
                    <div className="nav navbar-nav">
                      <li>
                        <a
                          className="nav-only-device"
                        >
                          Inscription
                        </a>
                      </li>
                      <li>
                        <a
                          className="nav-only-device"
                          data-toggle="collapse"
                          data-target="#bs-example-navbar-collapse-1"
                        >
                          Connexion
                        </a>
                      </li>
                    </div>
                  </ul>
                </div>
              </div>
              <div
                className="collapse navbar-collapse"
                id="bs-example-navbar-collapse-1"
              >
                <ul className="nav navbar-nav navbar-right">
                  <div ng-controller="inscriptionController">
                    <a className="btn btn-primary navbar-btn right">Inscription</a>{" "}
                    <a className="btn btn-strock-primary navbar-btn right">
                      Connexion
                    </a>
                  </div>
                </ul>
              </div>
            </div>
          </nav>
        </div>
        <div className="clearfix"></div>
      </div>
    </>
  );
}

export default DefaultHeader;
